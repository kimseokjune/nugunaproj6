package dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import vo.MakeHomepageVO;

public class MakeHomepageDAO {
	SqlSessionFactory sqlSessionFactory = MybatisConfig.getSqlSessionFactory();
	SqlSession sqlSession;

	public int makeHomepage(MakeHomepageVO vo) {

		int hosting_id = 0;
		try {
			sqlSession = sqlSessionFactory.openSession();
			sqlSession.insert("MakeHomepage.make", vo);
			sqlSession.commit();
			hosting_id = vo.getHosting_id();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}
		return hosting_id;

	}

	public void newAddress(MakeHomepageVO vo) {

		try {
			sqlSession = sqlSessionFactory.openSession();
			sqlSession.update("MakeHomepage.newAddress", vo);
			sqlSession.commit();
			System.out.println("dao 수정댐");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}

	}

	public int templateCheck(MakeHomepageVO vo) {
		int result = 0;
		try {
			sqlSession = sqlSessionFactory.openSession();
			String version = sqlSession.selectOne("MakeHomepage.templateCheck", vo);

			if (version != null) {
				result = Integer.parseInt(version) + 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}
		return result;
	}

}
