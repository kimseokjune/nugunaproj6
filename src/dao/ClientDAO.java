package dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import vo.Client;

public class ClientDAO {
	SqlSession sqlSession;
	SqlSessionFactory sqlSessionfactory = MybatisConfig.getSqlSessionFactory();

	// 회원가입용
	public boolean join(Client client) {
		boolean result = false;
		int row = 0;
		try {
			sqlSession = sqlSessionfactory.openSession();
			row = sqlSession.insert("Client.join", client);
			sqlSession.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}
		if (row == 1) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}

	// 일단 보류 비밀번호찾기 클릭시 먼저 아이디 값을 받는다.
	public Client searchId(String email) {
		Client client = new Client();
		System.out.println("dao 이메일넘어옴?" + email);
		try {
			sqlSession = sqlSessionfactory.openSession();
			client = sqlSession.selectOne("Client.searchId", email);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}
		return client;
	}

	// 3군데서 같이 쓴다.. 아이디 찾기 비밀번호 찾기 로그인하기
	// 로그인시 question 값에 따라 분기처리해준다
	// 로그인 시는 question 값이 login, 아이디 찾기 일 시는 question 값이 idsearch,
	// 비밀번호 찾기는 question 값이 passwordsearch
	public Client loginInfo(Client send_client) {
		Client return_client = null;
		try {
			System.out.println("DAO들어옴?" + send_client);
			sqlSession = sqlSessionfactory.openSession();
			return_client = sqlSession.selectOne("Client.login", send_client);
			System.out.println("아이디찾기" + return_client);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}
		// 임시로 가짜값 넣어줌
		//
		return return_client;
	}

	public int modify(Client client) {
		int cnt = 0;
		try {
			sqlSession = sqlSessionfactory.openSession();
			cnt = sqlSession.update("Client.modify", client);
			sqlSession.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}

		return cnt;
	}

	public Client modify_form(String email) {
		System.out.println("modify_form 들어옴");
		Client client = new Client();
		try {
			sqlSession = sqlSessionfactory.openSession();
			client = sqlSession.selectOne("Client.modify_form", email);
			System.out.println("client 나옴/" + client);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}

		return client;
	}

	public Client password_search(Client send_client) {
		Client return_client = null;
		try {
			System.out.println("DAO들어옴?" + send_client);
			sqlSession = sqlSessionfactory.openSession();
			return_client = sqlSession.selectOne("Client.password_search", send_client);
			System.out.println("아이디찾기" + return_client);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}
		return return_client;
	}

	public Client idsearch(Client send_client) {
		Client return_client = null;
		try {
			System.out.println("DAO들어옴?" + send_client);
			sqlSession = sqlSessionfactory.openSession();
			return_client = sqlSession.selectOne("Client.id_search", send_client);
			System.out.println("아이디찾기" + return_client);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}
		return return_client;
	}

	public void out(Client client) {
		int count = 0;
		try {
			sqlSession = sqlSessionfactory.openSession();
			count = sqlSession.delete("Client.out", client);

			sqlSession.commit();
		} 
		
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}

	}

}
