package dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import vo.MakeHomepageVO;

public class DomainDAO {
	SqlSession sqlSession;
	SqlSessionFactory sqlSessionfactory = MybatisConfig.getSqlSessionFactory();

	public MakeHomepageVO domainSearch(String domain) {
		System.out.println("dao에서 도매인은?"+domain);
		MakeHomepageVO vo = new MakeHomepageVO();
		try {
			sqlSession = sqlSessionfactory.openSession();
			 vo = sqlSession.selectOne("Domain.searchDomain", domain);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}

		return vo;
	}

	
	
	public String searchD(String domain) {
		System.out.println("dao에서 도매인은?"+domain);
		String domain_r = "";
		try {
			sqlSession = sqlSessionfactory.openSession();
			 domain_r = sqlSession.selectOne("Domain.searchD", domain);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}

		return domain_r;
	}
}
