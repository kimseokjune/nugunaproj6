package dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;

import vo.Board;

public class BoardDAO {
	SqlSession sqlSession = MybatisConfig.getSqlSessionFactory().openSession();
	
	public List<Board> selectBoardList(String searchField, String searchText, int startRecord, int countPerPage){
		HashMap<String,String> map = new HashMap<>();
		map.put("searchfield", searchField);
		map.put("searchtext", searchText);
		
		//결과 레코드 중 읽을 위치와 개수
		RowBounds bound = new RowBounds(startRecord, countPerPage);
		List<Board> boardList = sqlSession.selectList("mapper.BoardMapper.selectBoardList",map,bound);
		return boardList;
	}
	public int getTotal(String searchField,String searchText){
		int total = 0;
		HashMap<String, String> map = new HashMap<>();
		map.put("searchfield", searchField);
		map.put("searchtext", searchText);
		total = sqlSession.selectOne("mapper.BoardMapper.getTotal", map);
		System.out.println("daodaodao");
		System.out.println(total);
		return total;
	}
	public int insertBoard(Board board){
		int result = sqlSession.insert("mapper.BoardMapper.insertBoard",board);
		sqlSession.commit();
		return result;
	}
	
	public Board selectBoard(int boardno){System.out.println("selectBoard"+boardno);
		Board board = sqlSession.selectOne("mapper.BoardMapper.selectBoard",boardno);
		System.out.println("selectBoard"+board);
		sqlSession.update("mapper.BoardMapper.hitsUpdate",boardno);
		sqlSession.commit();
		return board;
	}
	public int updateBoard(Board board){
		int result = sqlSession.update("mapper.BoardMapper.updateBoard",board);
		sqlSession.commit();
		return result;
	}
	public int deleteBoard(int boardno){
		int result = sqlSession.delete("mapper.BoardMapper.deleteBoard",boardno);
		sqlSession.commit();
		return result;
	}
}
