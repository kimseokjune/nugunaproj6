package dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import vo.HomePageManageVO;
import vo.MakeHomepageVO;

public class HomePageManageDAO {
	SqlSessionFactory sqlSessionFactory = MybatisConfig.getSqlSessionFactory();
	SqlSession sqlSession;

	public List<MakeHomepageVO> homepageList(String owner) {
		List<MakeHomepageVO> list = new ArrayList<MakeHomepageVO>();
		try {
			sqlSession = sqlSessionFactory.openSession();
			list = sqlSession.selectList("HomePageManage.homepageList",owner);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null)
				sqlSession.close();
		}
		return list;
	}

}
