package action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

import dao.HomePageManageDAO;
import vo.HomePageManageVO;
import vo.MakeHomepageVO;

public class HomePageManageAction extends ActionSupport implements SessionAware {

	private int hosting_id;
	private String hosting_month;
	private String hosting_pay;
	private String cardnumber;
	private Date Pay_date;
	
	

	Map<String, Object> session;
	List<MakeHomepageVO> List;

	public String homepageList() {

		HomePageManageDAO dao = new HomePageManageDAO();
		List = dao.homepageList((String) session.get("email"));
		for (int i = 0; i < List.size(); i++) {
			System.out.println(List.get(i));
		}
		
		
		
		return SUCCESS;
	}
	
	public String editHomepage(){
		return SUCCESS;	
	}

	public List<MakeHomepageVO> getList() {
		return List;
	}

	public void setList(List<MakeHomepageVO> list) {
		List = list;
	}

	public int getHosting_id() {
		return hosting_id;
	}

	public void setHosting_id(int hosting_id) {
		this.hosting_id = hosting_id;
	}

	public String getHosting_month() {
		return hosting_month;
	}

	public void setHosting_month(String hosting_month) {
		this.hosting_month = hosting_month;
	}

	public String getHosting_pay() {
		return hosting_pay;
	}

	public void setHosting_pay(String hosting_pay) {
		this.hosting_pay = hosting_pay;
	}

	public String getCardnumber() {
		return cardnumber;
	}

	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}

	public Date getPay_date() {
		return Pay_date;
	}

	public void setPay_date(Date pay_date) {
		Pay_date = pay_date;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

}
