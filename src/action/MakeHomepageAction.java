package action;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import dao.DomainDAO;
import dao.MakeHomepageDAO;
import vo.MakeHomepageVO;

public class MakeHomepageAction extends ActionSupport {

	private String owner;
	private String title;
	private String shopname;
	private String shopaddr;
	private String shoptel;
	private String shopnumber;
	private String domain;
	private Date hosting_start;
	private Date hosting_end;
	private String templatename;
	private int version;
	private int hosting_id;

	private String html;
	private String rehtml;
	private String encording;


	private String fileName; // 동적생성되는 파일명 변수

	public String makeHomepage() {

		MakeHomepageVO vo = new MakeHomepageVO();
		vo.setOwner(getOwner());
		vo.setTitle(getTitle());
		vo.setShopname(getShopname());
		vo.setShopaddr(getShopaddr());
		vo.setShoptel(getShoptel());
		vo.setShopnumber(getShopnumber());
		vo.setDomain(getDomain());
		vo.setTemplatename(getTemplatename());

		
		//Hosting_id를 만든다.
		MakeHomepageDAO dao = new MakeHomepageDAO();
		vo.setHosting_id(dao.makeHomepage(vo));
		setHosting_id(vo.getHosting_id());
		
		return SUCCESS;
	}
	
	public String newAddress() {
		System.out.println("newAddress 액션은옴?");
		MakeHomepageVO vo = new MakeHomepageVO();
		vo.setShopaddr(getShopaddr());
		vo.setHosting_id(getHosting_id());
		//Hosting_id를 만든다.
		MakeHomepageDAO dao = new MakeHomepageDAO();
		System.out.println("액션에서 vo 찍어보기"+vo);
		dao.newAddress(vo);
		
		return SUCCESS;
	}
	
	
	
	public String indexHtmlsave(){
		fileName =  getHosting_id()+ "_Edit.jsp";
		System.out.println(fileName);
		return SUCCESS;
	}

	public String reHtmlSave() {
		
/*		System.out.println(getRehtml());
*/
		HttpServletRequest request = ServletActionContext.getRequest();
		String outFileName = request.getRealPath("/") + "makeHomepage\\template\\ownerPage\\" + getHosting_id()+ "_Index.jsp";	
		String jquery = "<script src=&#34;https://code.jquery.com/jquery-1.12.4.js&#34;></script>"+
"<script src=&#34;https://code.jquery.com/ui/1.12.1/jquery-ui.js&#34;></script>"
+"<script src=&#34;../script/editable.js&#34;></script>"
+"<link rel=&#34;stylesheet&#34; href=&#34;http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css&#34;>";
		
		
		FileWriter reader;
		try {
			reader = new FileWriter(outFileName);
			reader.write(StringEscapeUtils.unescapeHtml3(getEncording()) + getRehtml());
			reader.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		System.out.println("Index버전 저장완료:"+ outFileName );
		return SUCCESS;
		
		

/*		HttpServletRequest request = ServletActionContext.getRequest();
		String inFileName = request.getRealPath("/") + "makeHomepage\\ownerPage\\" + vo.getHosting_id()+ "_Edit.jsp" ;
		String outFileName = request.getRealPath("/") +"makeHomepage\\ownerPage\\" + vo.getHosting_id()+ "_Index.jsp" ;

		fileName = "\\ownerPage\\"+ vo.getHosting_id()+ "_edit.jsp";
		
		try {
			FileInputStream fis = new FileInputStream(inFileName);
			FileOutputStream fos = new FileOutputStream(outFileName);

			int data = 0;
			while ((data = fis.read()) != -1) {
				fos.write(data);
			}
			fis.close();
			fos.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("편집버전파일생성완료: " + outFileName);*/
	}

	public String htmlsave() {

		// System.out.println(getHtml());

		HttpServletRequest request = ServletActionContext.getRequest();
		String outFileName = request.getRealPath("/") + "makeHomepage\\template\\ownerPage\\" + getHosting_id()+ "_Edit.jsp";	

		String jquery = "<script src=&#34;https://code.jquery.com/jquery-1.12.4.js&#34;></script>"+
"<script src=&#34;https://code.jquery.com/ui/1.12.1/jquery-ui.js&#34;></script>"
+"<script src=&#34;../script/editable.js&#34;></script>"
+"<link rel=&#34;stylesheet&#34; href=&#34;http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css&#34;>";
		
		
		FileWriter reader;
		try {
			reader = new FileWriter(outFileName);
			reader.write(StringEscapeUtils.unescapeHtml3(getEncording())+StringEscapeUtils.unescapeHtml3(jquery) + getHtml());
			reader.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		System.out.println("Edit버전 저장완료:"+ outFileName );
		return SUCCESS;
	}

	public String goHomepage() {
		return SUCCESS;
	}
	
	public String domainCheck() {
		DomainDAO manager = new DomainDAO();
		String domain_r = manager.searchD(domain);
		setDomain(domain_r);
			return SUCCESS;
	}

	public String edit(){
/*		fileName = "template/ownerPage/"+getHosting_id()+"_Index.jsp";
		System.out.println(fileName);*/
		return SUCCESS;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShopname() {
		return shopname;
	}

	public void setShopname(String shopname) {
		this.shopname = shopname;
	}

	public String getShopaddr() {
		return shopaddr;
	}

	public void setShopaddr(String shopaddr) {
		this.shopaddr = shopaddr;
	}

	public String getShoptel() {
		return shoptel;
	}

	public void setShoptel(String shoptel) {
		this.shoptel = shoptel;
	}

	public String getShopnumber() {
		return shopnumber;
	}

	public void setShopnumber(String shopnumber) {
		this.shopnumber = shopnumber;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Date getHosting_start() {
		return hosting_start;
	}

	public void setHosting_start(Date hosting_start) {
		this.hosting_start = hosting_start;
	}

	public Date getHosting_end() {
		return hosting_end;
	}

	public void setHosting_end(Date hosting_end) {
		this.hosting_end = hosting_end;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public String getEncording() {
		return encording;
	}

	public void setEncording(String encording) {
		this.encording = encording;
	}

	public String getTemplatename() {
		return templatename;
	}

	public void setTemplatename(String templatename) {
		this.templatename = templatename;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getHosting_id() {
		return hosting_id;
	}

	public void setHosting_id(int hosting_id) {
		this.hosting_id = hosting_id;
	}

	public String getRehtml() {
		return rehtml;
	}

	public void setRehtml(String rehtml) {
		this.rehtml = rehtml;
	}
}
