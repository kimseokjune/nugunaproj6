package action;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import dao.ClientDAO;
import vo.Client;

public class ClientAction extends ActionSupport implements SessionAware {
	private Client client;
	private String result;
	private String password;
	private String name;
	private String question;
	Map<String, Object> session;
	private String email;
	private String address;
	
	
	
	

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	//회원탈퇴 입니다.
	public String out(){
		System.out.println("회원탈퇴 들어옴");
		ClientDAO manager = new ClientDAO();
		client.setEmail(email);
		System.out.println(client);
		manager.out(client);
		this.logout();
		return SUCCESS;
	}

	public String idcheck() {
		System.out.println("IDCHECK 들어옴");
		System.out.println(client.toString());
		ClientDAO manager = new ClientDAO();
		Client result_c = new Client();
		result_c = manager.searchId(client.getEmail());
		if (result_c != null) {
			System.out.println("있음");
			question = result_c.getQuestion();
			result = "no";
		} else {
			System.out.println("없음");
			result = "ok";
		}
		return SUCCESS;

	}

	// 아이디검색을 한다 . 아이디 검색을 위해서 Address 값에 idsearch를 넣어준다.
	public String idsearchresult() {
		System.out.println("아이디 검색결과 들어옴");
		System.out.println("들어옹");
		ClientDAO manager = new ClientDAO();
		System.out.println("클라이언트는?" + client.toString());
		client = manager.idsearch(client);
		if (client != null) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	// 로그인을 한다. 로그인을 위해서 Address 값에 login을 넣어준다
	public String login() {
		System.out.println("로그인 들어옴");
		ClientDAO manager = new ClientDAO();
		Client return_client = null;
		Client client = new Client();
		client.setEmail(email);
		client.setPassword(password);
		System.out.println("action에서 보내는 client" + client);
		return_client = manager.loginInfo(client);

		if (return_client != null) {
			ActionContext context = ActionContext.getContext();
			String currentURI = (String)context.getSession().get("login");
			System.out.println("액션에서 액션값 나옴?"+currentURI);
			if(currentURI == null){
				setAddress("../index/index.action");
				System.out.println(address);
			}
			else if(currentURI.equals("makeHomepageForm")){
				setAddress("../makeHomepage/makeHomepageForm.action");
			}
			else if(currentURI.equals("customerMenu")){
				setAddress("../customerMenu/customerMenu.action");
			}
			else if(currentURI.equals("QNABoardWriteForm")){
				setAddress("../board/QNABoardWriteForm.action");
			}
			session.put("email", return_client.getEmail());
			session.put("name", return_client.getName());
			name = return_client.getName();
			email = return_client.getEmail();
			result = "ok";
			client = return_client;
			return SUCCESS;

		} else {
			System.out.println("없다");
			result = "no";
			return ERROR;
		}

	}

	public String logout() {
		session.clear();
		return SUCCESS;
	}

	public String main() {

		return SUCCESS;
	}

	// 비밀번호 찾기를 한다. Address값에 passwordsearch 를 넣어준다.
	public String password_ok() {
		System.out.println("비밀번호 검색 들어옴");
		client.setEmail(email);
		client.setQuestion(question);
		System.out.println(client.toString());
		client.setAddress("passwordsearch");// 한 메서드를 사용하는데 구분값으로 사용
		ClientDAO manager = new ClientDAO();
		Client m_client = manager.password_search(client);
		client = m_client;
		if (client != null) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	public String loginForm() {

		return SUCCESS;
	}

	public String join() {
		System.out.println("회원가입 들어옴");
		System.out.println("아이디는" + email);
		client.setEmail(email);
		client.setPassword(password);
		System.out.println("questi?" + client.getQuestion());
		if (client.getQuestion().equals("직접입력")) {
			System.out.println("직접입력");
			client.setQuestion(question);
		}
		System.out.println("조인" + client.toString());
		ClientDAO manager = new ClientDAO();

		if (manager.join(client)) {
			return SUCCESS;
		} else {
			return ERROR;
		}
	}

	public String modify_form() {
		System.out.println("수정폼 들어옴");
		String email1 = (String) session.get("email");
		System.out.println("세션값잘나옴?" + email1);
		ClientDAO manager = new ClientDAO();

		client = manager.modify_form(email1);
		System.out.println(client);

		return SUCCESS;
	}

	public String modify() {
		System.out.println("수정 들어옴");
		client.setEmail(email);
		client.setPassword(password);
		System.out.println("수정하기" + client.toString());
		ClientDAO manager = new ClientDAO();
		int cnt = manager.modify(client);
		System.out.println("숫자는?" + cnt);
		if (cnt == 1) {
			return SUCCESS;

		} else {
			return ERROR;
		}
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

}
