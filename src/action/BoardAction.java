package action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

import dao.BoardDAO;
import util.PageNavigator;
import vo.Board;

public class BoardAction extends ActionSupport implements SessionAware{
	Board board;
	BoardDAO dao = new BoardDAO();
	List<Board> boardlist;
	Map<String,Object> session;
	int boardno;
	
	PageNavigator pagenavi;	//페이징 관련 정보
	int currentPage = 1;	//현재 페이지 초기값
	String searchField;		//검색 키워드 종류
	String searchText;		//검색 키워드
	
	public String QNABoardList(){
		int countPerPage = Integer.parseInt(getText("board.countperpage"));
		int pagePerGroup = Integer.parseInt(getText("board.pagepergroup"));
		BoardDAO dao = new BoardDAO();
		
		System.out.println("검색필드:"+searchField+" 검색키워드:"+searchText);
		//전체글수구하기
		int total = dao.getTotal(searchField,searchText);
		
		//PageNavigator 객체 생성
		pagenavi = new PageNavigator(countPerPage, pagePerGroup, currentPage, total);
		
		//현재 페이지에 해당하는 글목록 읽기
		//
		System.out.println(session.get("loginid"));
		boardlist = dao.selectBoardList(searchField,searchText,pagenavi.getStartRecord(),pagenavi.getCountPerPage());
		return SUCCESS;
	}
	public String QNABoardWrite(){
		
		//board.setEmail((String) session.get("loginid"));
		//board.setName((String) session.get("name"));
		board.setBoardtype("qna");
		dao.insertBoard(board);
		return SUCCESS;
	}
	public String QNABoardWriteForm(){
		return SUCCESS;
	}
	public String QNABoardView(){
		System.out.println("BoardViewStarted");
		board = dao.selectBoard(boardno);
		System.out.println("BoardView"+board);
		return SUCCESS;
	}
	public String QNABoardUpdateForm(){
		board = dao.selectBoard(boardno);
		return SUCCESS;
	}
	public String QNABoardUpdate(){
		board.setBoardno(boardno);
		dao.updateBoard(board);
		return SUCCESS;
	}
	public String QNABoardDelete(){
		System.out.println(boardno);
		dao.deleteBoard(boardno);
		return SUCCESS;
	}
	
	//get set
	public Board getBoard() {
		return board;
	}
	public void setBoard(Board board) {
		this.board = board;
	}
	public List<Board> getBoardlist() {
		return boardlist;
	}
	public void setBoardlist(List<Board> boardlist) {
		this.boardlist = boardlist;
	}
	
	public int getBoardno() {
		return boardno;
	}
	public void setBoardno(int boardno) {
		this.boardno = boardno;
	}
	
	public PageNavigator getPagenavi() {
		return pagenavi;
	}
	public void setPagenavi(PageNavigator pagenavi) {
		this.pagenavi = pagenavi;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public String getSearchField() {
		return searchField;
	}
	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	@Override
	public void setSession(Map<String, Object> arg0) {
		session = arg0;
	}
	
	
	
	
	
}
