package interceptor;

import java.util.Map;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class LoginInterceptor extends AbstractInterceptor {

   @Override
   public String intercept(ActionInvocation invokation) throws Exception {
	      ActionContext ctx = invokation.getInvocationContext();
	      Map<String, Object> session = ctx.getSession();
	      System.out.println(ctx.getName());
	      System.out.println(ctx.getContext());
	      String loginId = (String) session.get("email");
	      if(loginId == null){
	    	  String actionName = invokation.getInvocationContext().getName();
	    	  session.put("login", actionName);
	    	  System.out.println("인터셉터에서 액션이름"+actionName);
	         return Action.LOGIN;
	      }
	      String invo = invokation.invoke(); 
	      System.out.println("후인터"+invo);
	      return invo; 
	   }

	}