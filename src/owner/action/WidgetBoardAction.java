package owner.action;

import java.util.ArrayList;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

import owner.dao.WidgetBoardDAO;
import owner.vo.Widget_board;
import util.PageNavigator;

public class WidgetBoardAction extends ActionSupport implements SessionAware{
	Widget_board board = new Widget_board();
	Map<String, Object> session;
	WidgetBoardDAO dao = new WidgetBoardDAO();
	ArrayList<Widget_board> list=null;
	int wbno;
	String wbpass;
	String id;
	int delres;
	ArrayList<Integer> listCnt;
	PageNavigator pagenavi;
	int currentPage = 1;
	int total;
/*	int boardno;
	String rewrite;
	String searchField;
	String search;*/

	
/*	private File upload;
	private String uploadFileName;
	private String uploadContentType;*/
	
	
	public String widgetboard_session(){
		
		return SUCCESS;
	}
	
	public String widgetboard_list(){
		int countPerPage = Integer.parseInt(getText("board.countperpage"));
		int pagePerGroup = Integer.parseInt(getText("board.pagepergroup"));
		//토탈페이지 구하는 db추가
		
		/*total = dao.getTotal(searchField, search);*/
		
		pagenavi = new PageNavigator(countPerPage, pagePerGroup, currentPage, total);
		list = dao.listBoard(board,  board.getWbtype(), 1, 1);
		return SUCCESS;
	}
	public String widgetboard_noti(){
		System.out.println("리스트 jsp아이디: "+board.getEmail());
		list = dao.listBoard(board,  board.getWbtype(), 1, 1);
		return SUCCESS;
	}
	
	public String widgetboard_up(){
		System.out.println("삭제: " +board);
		dao.deleteBoard(wbno);
		list = dao.listBoard(board,  board.getWbtype(), 1, 1);
		return SUCCESS;
	}
	
	public String linedeleteBoard(){
		int result= dao.linedeleteBoard(wbno, wbpass);
		delres = result;	
		list = dao.listBoard(board,  board.getWbtype(), 1, 1);
			return SUCCESS;
		
	}
	
	public String widgetboardinsert(){
		System.out.println("입력: "+board);
		dao.widgetinsertBoard(board);
		list = dao.listBoard(board,  board.getWbtype(), 1, 1);
		return SUCCESS;
	}
	
	
	/*public String insert_board() throws Exception{
		String loginId = (String) session.get("se_id");
		board.setCustid(loginId);
		
		if(upload != null){
			FileService fs = new FileService();
			String basePath = getText("board.uploadpath");
			String savefile = fs.saveFile(upload, basePath, uploadFileName);
			board.setSavedfile(savefile);
			board.setOriginalfile(uploadFileName);
		}else{
		board.setSavedfile(" ");
		board.setOriginalfile(" ");
		}
		dao.insertBoard(board);
		return SUCCESS;
	}

	public String update_board(){
		dao.updateBoard(board);
		return SUCCESS;
	}
	
	public String read_board(){
		board = dao.noBoard(boardno);
		if(board ==null) return INPUT;
		dao.hitUpdate(boardno);
		return SUCCESS;
	}
	
	public String delete_board(){
		dao.deleteBoard(boardno);
		return SUCCESS;
	}
	
	
	public Board getBoard() {
		return board;
	}*/



	/*public void setBoard(Board board) {
		this.board = board;
	}



	public ArrayList<Board> getList() {
		return list;
	}



	public void setList(ArrayList<Board> list) {
		this.list = list;
	}

*/


	/*public String getSearchField() {
		return searchField;
	}*/



	public Widget_board getBoard() {
		return board;
	}

	public int getDelres() {
		return delres;
	}

	public void setDelres(int delres) {
		this.delres = delres;
	}

	public String getWbpass() {
		return wbpass;
	}

	public void setWbpass(String wbpass) {
		this.wbpass = wbpass;
	}

	public void setBoard(Widget_board board) {
		this.board = board;
	}

	
	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}


	public WidgetBoardDAO getDao() {
		return dao;
	}

	public void setDao(WidgetBoardDAO dao) {
		this.dao = dao;
	}

	public ArrayList<Widget_board> getList() {
		return list;
	}

	public void setList(ArrayList<Widget_board> list) {
		this.list = list;
	}

	
	
/*	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}



	public String getSearch() {
		return search;
	}



	public void setSearch(String search) {
		this.search = search;
	}



	public int getBoardno() {
		return boardno;
	}

	public void setBoardno(int boardno) {
		this.boardno = boardno;
	}

	
	
	public String getRewrite() {
		return rewrite;
	}

	public void setRewrite(String rewrite) {
		this.rewrite = rewrite;
	}*/

	
	
	public PageNavigator getPagenavi() {
		return pagenavi;
	}

	public void setPagenavi(PageNavigator pagenavi) {
		this.pagenavi = pagenavi;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}


	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	
	public ArrayList<Integer> getListCnt() {
		return listCnt;
	}

	public void setListCnt(ArrayList<Integer> listCnt) {
		this.listCnt = listCnt;
	}

	
	
	/*public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public String getUploadContentType() {
		return uploadContentType;
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}*/

	public int getWbno() {
		return wbno;
	}

	public void setWbno(int wbno) {
		this.wbno = wbno;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
		
	}

}
