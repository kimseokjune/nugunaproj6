package owner.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import dao.MybatisConfig;
import owner.vo.Widget_board;

public class WidgetBoardDAO {
	SqlSession sqlSession = MybatisConfig.getSqlSessionFactory().openSession();//데이타데이스에 연결
	
	public void widgetinsertBoard(Widget_board bo){
	
		sqlSession.insert("ownerMapper.widgetBoardMapper.insertBoard", bo);
		sqlSession.commit();
		
	}
	/*
	public void updateBoard(Widget_board bo){
		sqlSession.update("mapper.BoardMapper.updateBoard", bo);
		sqlSession.commit();
		
	}
	
	public int getTotal(String searchField, String searchText){
		Map<String, Object> map = new HashMap<>();
		map.put("searchField", searchField);
		map.put("searchText", searchText);
		int result = sqlSession.selectOne("mapper.BoardMapper.getTotal", map);
		return result;
	}*/
	
	public ArrayList<Widget_board> listBoard(Widget_board bo,String wbtype, int startRecord, int counterPage){
		ArrayList<Widget_board> board = new ArrayList<>();
/*		Map<String, Object> map = new HashMap<>();
		map.put("email", email);
		map.put("wbtype", wbtype);*/
		
		RowBounds bound = new RowBounds(startRecord, counterPage); //mybatis 제공되는 row계산해줌 기존쿼리를 간단하게
		List<Widget_board> li = sqlSession.selectList("ownerMapper.widgetBoardMapper.selectBoard", bo);
		board = (ArrayList<Widget_board>)li;
		return board;
	}
	//새로운
/*	public ArrayList<Board> listBoard(String searchField,String searchText, int startRecord, int counterPage){
			RowBounds bound = new RowBounds(startRecord, counterPage); //mybatis 제공되는 페이지그룹화
		return null;
	}*/
	
/*	public Widget_board noBoard(int boardno){
		Widget_board board = sqlSession.selectOne("mapper.BoardMapper.noBoard", boardno);
		
		return board;
	}
	*/
	public void deleteBoard(int wbno){
		sqlSession.delete("ownerMapper.widgetBoardMapper.deleteBoard", wbno);
		sqlSession.commit();
	}
	
	public int linedeleteBoard(int wbno, String wbpass){
		Map<String, Object> map = new HashMap<>();
		map.put("wbno", wbno);
		map.put("wbpass", wbpass);
		System.out.println("전단계");
		int result = sqlSession.delete("ownerMapper.widgetBoardMapper.linedeleteBoard", map);
		System.out.println(result);
		if(result == 0){
			return 0;
		}
		else{
			sqlSession.commit();
			return 1;
			}
		}
	}
	
