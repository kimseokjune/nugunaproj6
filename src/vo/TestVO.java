package vo;

public class TestVO {
	private String testStr;
	private int testInt;
	
	
	public String getTestStr() {
		return testStr;
	}
	public void setTestStr(String testStr) {
		this.testStr = testStr;
	}
	public int getTestInt() {
		return testInt;
	}
	public void setTestInt(int testInt) {
		this.testInt = testInt;
	}
	@Override
	public String toString() {
		return "TestVO [testStr=" + testStr + ", testInt=" + testInt + "]";
	}
	
	
}
