package vo;

import java.util.Calendar;
import java.util.Date;

public class MakeHomepageVO {

	private String owner;
	private String title;
	private String shopname;
	private String shopaddr;
	private String shoptel;
	private String shopnumber;
	private String templatename;
	private int version;
	private String domain;
	private Date hosting_start;
	private Date hosting_end;
	private String hosting_day; //�몄�ㅽ���⑥����吏�怨���
	
	private int hosting_id;
	

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShopname() {
		return shopname;
	}

	public void setShopname(String shopname) {
		this.shopname = shopname;
	}

	public String getShopaddr() {
		return shopaddr;
	}

	public void setShopaddr(String shopaddr) {
		this.shopaddr = shopaddr;
	}

	public String getShoptel() {
		return shoptel;
	}

	public void setShoptel(String shoptel) {
		this.shoptel = shoptel;
	}

	public String getShopnumber() {
		return shopnumber;
	}

	public void setShopnumber(String shopnumber) {
		this.shopnumber = shopnumber;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Date getHosting_start() {
		return hosting_start;
	}

	public void setHosting_start(Date hosting_start) {
		this.hosting_start = hosting_start;
	}

	public Date getHosting_end() {
		return hosting_end;
	}

	public void setHosting_end(Date hosting_end) {
		this.hosting_end = hosting_end;
	}

	public String getTemplatename() {
		return templatename;
	}

	public void setTemplatename(String templatename) {
		this.templatename = templatename;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getHosting_id() {
		return hosting_id;
	}

	public void setHosting_id(int hosting_id) {
		this.hosting_id = hosting_id;
	}

	public String getHosting_day() {
		return hosting_day;
	}

	public void setHosting_day(String hosting_day) {
		this.hosting_day = hosting_day;
	}

	@Override
	public String toString() {
		return "MakeHomepageVO [owner=" + owner + ", title=" + title + ", shopname=" + shopname + ", shopaddr=" + shopaddr + ", shoptel=" + shoptel + ", shopnumber=" + shopnumber + ", templatename="
				+ templatename + ", version=" + version + ", domain=" + domain + ", hosting_start=" + hosting_start + ", hosting_end=" + hosting_end + ", hosting_day=" + hosting_day + ", hosting_id="
				+ hosting_id + "]";
	}

	
}
