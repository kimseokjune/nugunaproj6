package vo;

public class Test1VO {
	private String name;
	private String age;
	private String addr;
	
	public Test1VO(){}
	public Test1VO(String name, String age, String addr) {
		this.name = name;
		this.age = age;
		this.addr = addr;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	@Override
	public String toString() {
		return "Test1VO [name=" + name + ", age=" + age + ", addr=" + addr + "]";
	}
	
	
}
