package vo;

import java.util.Date;

public class HomePageManageVO {
	
	//manage
	private int hosting_id;
	private String hosting_month;
	private String hosting_pay;
	private String cardnumber;
	private Date Pay_date;

	
	//client
	private String email;
	private String password;
	private String name;
	private String phone_number;
	private String address;
	private String license_number;
	private String question;
	private String answer;
	private String grade;
	
	
	//makehomepage
	private String owner;
	private String title;
	private String shopname;
	private String shopaddr;
	private String shoptel;
	private String shopnumber;
	private String templatename;
	private int version;
	private String domain;
	private Date hosting_start;
	private Date hosting_end;
	

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShopname() {
		return shopname;
	}

	public void setShopname(String shopname) {
		this.shopname = shopname;
	}

	public String getShopaddr() {
		return shopaddr;
	}

	public void setShopaddr(String shopaddr) {
		this.shopaddr = shopaddr;
	}

	public String getShoptel() {
		return shoptel;
	}

	public void setShoptel(String shoptel) {
		this.shoptel = shoptel;
	}

	public String getShopnumber() {
		return shopnumber;
	}

	public void setShopnumber(String shopnumber) {
		this.shopnumber = shopnumber;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Date getHosting_start() {
		return hosting_start;
	}

	public void setHosting_start(Date hosting_start) {
		this.hosting_start = hosting_start;
	}

	public Date getHosting_end() {
		return hosting_end;
	}

	public void setHosting_end(Date hosting_end) {
		this.hosting_end = hosting_end;
	}

	public String getTemplatename() {
		return templatename;
	}

	public void setTemplatename(String templatename) {
		this.templatename = templatename;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLicense_number() {
		return license_number;
	}
	public void setLicense_number(String license_number) {
		this.license_number = license_number;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	
	
	public int getHosting_id() {
		return hosting_id;
	}
	public void setHosting_id(int hosting_id) {
		this.hosting_id = hosting_id;
	}
	public String getHosting_month() {
		return hosting_month;
	}
	public void setHosting_month(String hosting_month) {
		this.hosting_month = hosting_month;
	}
	public String getHosting_pay() {
		return hosting_pay;
	}
	public void setHosting_pay(String hosting_pay) {
		this.hosting_pay = hosting_pay;
	}
	public String getCardnumber() {
		return cardnumber;
	}
	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}
	public Date getPay_date() {
		return Pay_date;
	}
	public void setPay_date(Date pay_date) {
		Pay_date = pay_date;
	}


}
