package vo;

public class Board {
	private int boardno;
	private String boardtype;
	private String email;
	private String title;
	private String content;
	private String inputdate;
	private int hits;
	private String replied;
	private String name;
	
	
	public Board() {
		
	}
	

	@Override
	public String toString() {
		return "Board [boardno=" + boardno + ", boardtype=" + boardtype + ", email=" + email + ", title=" + title
				+ ", content=" + content + ", inputdate=" + inputdate + ", hits=" + hits + ", replied=" + replied
				+ ", name=" + name + "]";
	}


	public int getBoardno() {
		return boardno;
	}


	public void setBoardno(int boardno) {
		this.boardno = boardno;
	}


	public String getBoardtype() {
		return boardtype;
	}


	public void setBoardtype(String boardtype) {
		this.boardtype = boardtype;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getInputdate() {
		return inputdate;
	}


	public void setInputdate(String inputdate) {
		this.inputdate = inputdate;
	}


	public int getHits() {
		return hits;
	}


	public void setHits(int hits) {
		this.hits = hits;
	}


	public String getReplied() {
		return replied;
	}


	public void setReplied(String replied) {
		this.replied = replied;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	
	
	
	
	
}
