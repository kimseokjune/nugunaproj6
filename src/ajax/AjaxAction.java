package ajax;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionSupport;

import vo.TestVO;

public class AjaxAction extends ActionSupport{
	private String str="strOne";
	private int intNum=0;
	private String[] strArray = {"a","b","c"};
	private int[] intArray ={1,2,3,4};
	private List<String> strList;
	private Map<String, String> strMap;
	private TestVO testVO;
	
	public String ajaxOne(){
		System.out.println(str);
		str = "img/aa.jpg";
		return SUCCESS;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	public int getIntNum() {
		return intNum;
	}

	public void setIntNum(int intNum) {
		this.intNum = intNum;
	}

	public String[] getStrArray() {
		return strArray;
	}

	public void setStrArray(String[] strArray) {
		this.strArray = strArray;
	}

	public int[] getIntArray() {
		return intArray;
	}

	public void setIntArray(int[] intArray) {
		this.intArray = intArray;
	}

	public List<String> getStrList() {
		return strList;
	}

	public void setStrList(List<String> strList) {
		this.strList = strList;
	}

	public Map<String, String> getStrMap() {
		return strMap;
	}

	public void setStrMap(Map<String, String> strMap) {
		this.strMap = strMap;
	}

	public TestVO getTestVO() {
		return testVO;
	}

	public void setTestVO(TestVO testVO) {
		this.testVO = testVO;
	}
	
	
	
	
}
