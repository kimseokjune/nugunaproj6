<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
	<meta name="author" content="pixelhint.com">
	<meta name="description" content="La casa free real state fully responsive html5/css3 home page website template"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
	
<!-- 	<link rel="stylesheet" type="text/css" href="../makeHomepage/template/La Casa/css/reset.css"> -->
	<link rel="stylesheet" type="text/css" href="../makeHomepage/template/La Casa/css/responsive.css">

	<!-- <script type="text/javascript" src="js/jquery.js"></script> -->
	<script type="text/javascript" src="../makeHomepage/template/La Casa/js/main.js"></script>
	<script type="text/javascript">
$(function(){
	$("#shop").append($("#shopname").val());
	$("title").attr("value", $("#shopname").val());
});
</script>
</head>
<body>

	<section class="hero">
		<header>
			<div class="wrapper" style="z-index:99;">
				<a href="#" ></a><h1 id="shop" class="logo" style="font-size:30px;color:white;vertical-align: middle;"></h1>
				<a href="#" class="hamburger"></a>
				<nav>
					<ul>
						<li><a href="#">Buy</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">Contact</a></li>
					</ul>
					
				</nav>
			</div>
		</header><!--  end header section  -->

			<section class="caption">
				<h2 class="caption">&nbsp;</h2>
				<h3 class="properties">&nbsp;</h3>
			</section>
	</section><!--  end hero section  -->





	<section class="listings">
		<div class="wrapper">
			<ul class="properties_list">
				<li>
					<a href="#">
						<img src="../makeHomepage/template/La Casa/img/property_1.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">$2500</span>
					<div class="property_details">
						<h1>
							<a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
						</h1>
						<h2>&nbsp;&nbsp;&nbsp;</h2><br>
					</div>
				</li>
				<li>
					<a href="#">
						<img src="../makeHomepage/template/La Casa/img/property_2.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">$1000</span>
					<div class="property_details">
						<h1>
							<a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
						</h1>
						<h2>&nbsp;&nbsp;&nbsp;</h2><br>
					</div>
				</li>
				<li>
					<a href="#">
						<img src="../makeHomepage/template/La Casa/img/property_3.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">$500</span>
					<div class="property_details">
						<h1>
							<a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
						</h1>
						<h2>&nbsp;&nbsp;&nbsp;</h2><br>
					</div>
				</li>
				<li>
					<a href="#">
						<img src="../makeHomepage/template/La Casa/img/property_1.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">$2500</span>
					<div class="property_details">
						<h1>
							<a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
						</h1>
						<h2>&nbsp;&nbsp;&nbsp;</h2><br>
					</div>
				</li>
				<li>
					<a href="#">
						<img src="../makeHomepage/template/La Casa/img/property_2.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">$1000</span>
					<div class="property_details">
						<h1>
							<a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
						</h1>
						<h2>&nbsp;&nbsp;&nbsp;</h2><br>
					</div>
				</li>
				<li>
					<a href="#">
						<img src="../makeHomepage/template/La Casa/img/property_3.jpg" alt="" title="" class="property_img"/>
					</a>
					<span class="price">$500</span>
					<div class="property_details">
						<h1>
							<a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
						</h1>
						<h2>&nbsp;&nbsp;&nbsp;</h2><br>
					</div>
				</li>
			
			</ul>
		</div>
	</section>	<!--  end listing section  -->
	<h1 style="text-align:center;">MAP</h1>
	<footer>

		<div class="wrapper footer">
			<ul>
				<li class="links">
					<ul>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>

				<li class="links">
					<ul>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>

				<li class="links">
					<ul>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>
				<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

				<li class="about">
					<p></p>
					<ul>
						<li><a href="http://facebook.com/pixelhint" class="facebook" target="_blank"></a></li>
						<li><a href="http://twitter.com/pixelhint" class="twitter" target="_blank"></a></li>
						<li><a href="http://plus.google.com/+Pixelhint" class="google" target="_blank"></a></li>
						<li><a href="#" class="skype"></a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="copyrights wrapper">
			Copyright © 2015 <a href="http://pixelhint.com" target="_blank" class="ph_link" title="Download more free Templates">Pixelhint.com</a>. All Rights Reserved.
		</div>
	</footer><!--  end footer  -->
	
</body>
</html>