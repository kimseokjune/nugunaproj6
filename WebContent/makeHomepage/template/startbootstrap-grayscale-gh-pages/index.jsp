<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html>
<script type="text/javascript">
$(function(){
	$("#shop").append($("#shopname").val());
	$("title").attr("value", $("#shopname").val());
});
</script>
<html>

<head>

    <title></title>

    <!-- Bootstrap Core CSS -->
    <link href="../makeHomepage/template/startbootstrap-grayscale-gh-pages/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> 

    <!-- Custom Fonts -->
    <link href="../makeHomepage/template/startbootstrap-grayscale-gh-pages/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <!-- Theme CSS -->
    <link href="../makeHomepage/template/startbootstrap-grayscale-gh-pages/css/grayscale.css" rel="stylesheet">

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <i class="fa fa-play-circle"></i> <span class="light" id="shop"></span>
	
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav" id="nv">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#download">Story</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

      <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading"></h1>
                        <p class="intro-text">
                            <br></p>
                        <a href="#about" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2></h2>
                <p></p>
                <p></p>
                <p></p>
            </div>
        </div>
    </section>

    <!-- Download Section -->
    <section id="download" class="content-section text-center">
      
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2></h2>
                    <p></p>
                    
                </div>
           
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2></h2>
                <p></p>
                <p>
                </p>
                <ul class="list-inline banner-social-buttons">
                    <li>
                        <a href="https://twitter.com/SBootstrap" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                    </li>
                    <li>
                        <a href="https://github.com/IronSummitMedia/startbootstrap" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                    </li>
                    <li>
                        <a href="https://plus.google.com/+Startbootstrap/posts" class="btn btn-default btn-lg"><i class="fa fa-google-plus fa-fw"></i> <span class="network-name">Google+</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <!-- Map Section -->
    <div id="map"></div>

    <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; Your Website 2016</p>
        </div>
    </footer>

    <!-- jQuery -->
<%--      <script src="vendor/jquery/jquery.js"></script>  --%>

    <!-- Bootstrap Core JavaScript -->
     <%-- <script src="vendor/bootstrap/js/bootstrap.min.js"></script>  --%>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
     <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

    <!-- Theme JavaScript -->
 <%--    <script src="js/grayscale.min.js"></script> --%>
    <div id="widgetcon">
    
<!-- <div id="BBB_0" name="BBB" class="text-container ui-draggable ui-draggable-handle" style="position: absolute; top: 239px; left: 363px; z-index: 5; width: 100%;"><div id="text-field" style="font-size: 46px; color: rgb(255, 255, 255);">私どものホームページに いらっしゃいませ。</div><div name="BBB_option" class="div_option" style="display: block; overflow: hidden; height: 5.2781px; padding: 0px; margin: 0px; width: 238.721px; opacity: 0.150803;"><label for="handleText_0"><img class="handleBtn"></label><input id="handleText_0" type="button" value="핸들링" style="display: none;"><label for="deleteText_0"><img class="deleteBtn"></label><input id="deleteText_0" type="button" value="삭제" style="display: none;"></div></div>
	
	
	<div id="AAA_0" name="AAA" class="image-container ui-draggable ui-draggable-handle ui-resizable" style="position: absolute; top: 168px; left: 648px; z-index: 5;"><img src="../menu/img/defaultpic.jpg" width="100%" height="100%" object-fit="contain"><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90; display: none;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90; display: none;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90; display: none;"></div><div name="AAA_option" class="div_option" style="display: none;"><label for="imageUp_0"><img class="imageBtn"></label><input id="imageUp_0" type="file" style="display: none;"><label for="deleteDiv_0"><img class="deleteBtn"></label><input id="deleteDiv_0" type="button" value="삭제" style="display: none;"></div></div>
     --></div>
	

</body>
</html>