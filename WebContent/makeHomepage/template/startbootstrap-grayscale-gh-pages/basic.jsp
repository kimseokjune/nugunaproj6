<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE html>
<html>
<head>
    <title></title>

    <!-- Bootstrap Core CSS -->
    <link href="../makeHomepage/template/startbootstrap-grayscale-gh-pages/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> 

    <!-- Custom Fonts -->
    <link href="../makeHomepage/template/startbootstrap-grayscale-gh-pages/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <!-- Theme CSS -->
    <link href="../makeHomepage/template/startbootstrap-grayscale-gh-pages/css/grayscale.css" rel="stylesheet">
 
<script type="text/javascript">
$("#shop").append($("#shopname").val());
$("title").attr("value", $("#shopname").val());


</script>

</head>


<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse" style="border: 0px dotted;">
                    Menu <i class="fa fa-bars" style="border: 0px dotted;"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top" style="border: 0px dotted;">
                    <i class="fa fa-play-circle" style="border: 0px dotted;"></i> <span class="light" id="shop" style="border: 0px dotted;"></span>
	
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav" style="border: 0px dotted;">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden" style="border: 0px dotted;">
                        <a href="#intro" style="border: 0px dotted;"></a>
                    </li>
                    <li style="border: 0px dotted;">
                        <a class="page-scroll" href="#top" style="border: 0px dotted;">About</a>
                    </li>
                    <li style="border: 0px dotted;">
                        <a class="page-scroll" href="#about" style="border: 0px dotted;">Story</a>
                    </li>
                    <li style="border: 0px dotted;">
                        <a class="page-scroll" href="#contact" style="border: 0px dotted;">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row" style="border: 0px dotted;">
                    <div class="col-md-8 col-md-offset-2" style="border: 0px dotted;">
               <!--      //�ؽ�Ʈ���� 1�� -->
   
                        <h1 class="brand-heading" style="border: 0px dotted;"></h1>
                        <a href="#about" class="btn btn-circle page-scroll" style="border: 0px dotted;">
                            <i class="fa fa-angle-double-down animated" style="border: 0px dotted;"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                
            </div>
        </div>
    </section>

       <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">

            </div>
        </div>
    </section>

    <!-- Map Section -->

    <!-- Footer -->


    <!-- jQuery -->



    <!-- Bootstrap Core JavaScript -->
 <%--  <script src="..\makeHomepage\template\startbootstrap-grayscale-gh-pages\vendor\bootstrap\js\bootstrap.min.js"></script> --%> 

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
 <%--    <script src="..\makeHomepage\template\startbootstrap-grayscale-gh-pages\js\grayscale.min.js"></script> --%>

	
</body>
</html>

