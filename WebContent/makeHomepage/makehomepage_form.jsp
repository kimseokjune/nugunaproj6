<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" href="css/style.css">
</head>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="../script/jquery-3.1.0.min.js"></script>
<script>
function share(){
	 var result = false;
			var id = $('#domain').val();
			$.ajaxSettings.traditional = true;
			
			$.ajax({
				url : "../makeHomepage/domainCheck",
				type : "post",
				async: false,
				data : {
					"domain" : id
				},
				success : function(data){
					if(data.domain == null){ 
					result = true;
					}else{
					alert('すでに存在するドメインです');
					result =false;
					}
					
				}
			});//end ajax
	return result;
}
</script>
<script>

	function sample6_execDaumPostcode() {
		new daum.Postcode({
			oncomplete : function(data) {
				// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

				// 각 주소의 노출 규칙에 따라 주소를 조합한다.
				// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
				var fullAddr = ''; // 최종 주소 변수
				var extraAddr = ''; // 조합형 주소 변수

				// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
				if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
					fullAddr = data.roadAddress;

				} else { // 사용자가 지번 주소를 선택했을 경우(J)
					fullAddr = data.jibunAddress;
				}

				// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
				if (data.userSelectedType === 'R') {
					//법정동명이 있을 경우 추가한다.
					if (data.bname !== '') {
						extraAddr += data.bname;
					}
					// 건물명이 있을 경우 추가한다.
					if (data.buildingName !== '') {
						extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
					}
					// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.

				}

				// 우편번호와 주소 정보를 해당 필드에 넣는다.
				document.getElementById('sample6_address').value = fullAddr;

				// 커서를 상세주소 필드로 이동한다.
				document.getElementById('sample6_address2').focus();
			}
		}).open();
	}
</script>
<body>
	<!--   메뉴시작  -->
	<div id="menu">
		<%@include file="../menu/menu.jsp"%>
	</div>
	<!--   메뉴종료  -->

	<!--   본문시작  -->
	<div id="content">

		<div id="hero">
			<div id="background_animate"></div>
			<h1>${session.name}さま!
				<br>どんなホームページを作るつもりですか?
			</h1>
		</div>
		<form  method="post"
			accept-charset="utf-8" name="list" id="list" action="makeHomepage" onsubmit="return share()">


			<div class="input_holder">
				<span>タイトル</span><input type="text" name="title" required="">
			</div>

			<div class="input_holder">
				<span>店の名前</span><input type="text" name="shopname" required="">
			</div>
			<div class="input_holder">
				<span></span> <input type="button"
					onclick="sample6_execDaumPostcode()" value="住所の検索"
					style="width: 150px; text-align: center;">
			</div>
			<div class="input_holder">
				<span>住所</span><input type="text" id="sample6_address"
					name="shopaddr" required="">
			</div>
			<div class="input_holder">
				<span>詳細住所</span><input type="text" id="sample6_address2"
					required="">
			</div>
			<div class="input_holder">
				<span>電話番号</span><input type="text" name="shoptel" required="">
			</div>

			<div class="input_holder">
				<span>事業者番号</span><input type="text" name="shopnumber" required="">
			</div>

			<div class="input_holder">
				<span>ドメイン</span><input type="text" name="domain" id="domain" required="">
			</div>
			<input type="submit" value="作り" class="submit_button" id="Shareitem"> <input
				type="hidden" name="owner" value="${session.email}"> <input
				type="hidden" name="templatename" value="basic">
		</form>
	</div>

	<!--   본문종료  -->


	<script
		src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script
		src='https://s3-us-west-2.amazonaws.com/s.cdpn.io/63425/transit.js'></script>
	<script
		src='https://s3-us-west-2.amazonaws.com/s.cdpn.io/63425/background.js'></script>

	<script src="js/index.js"></script>
</body>
</html>





<%-- 




		<form action="makeHomepage.action" method="post">
			<ul>
				<li>소유자:</li>
			
			
				<li>가게주소:<input type="text" name="shopaddr"></li>
				<li>가게전화번호:<input type="text" name="shoptel"></li>
				<li>사업자번호:<input type="text" name="shopnumber"></li>
				<li>도메인:<input type="text" name="domain"></li>
			</ul>
			<input type="submit" value="만들기">
		</form> --%>