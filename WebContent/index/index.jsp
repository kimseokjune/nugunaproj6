<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html>
<html class="no-js">
<head>
<title>NUGUNA</title>
<link rel="stylesheet" href="glow/css/animate.css">
<link rel="stylesheet" href="glow/css/icomoon.css">
<link rel="stylesheet" href="glow/css/bootstrap.css">
<link rel="stylesheet" href="glow/css/magnific-popup.css">
<link rel="stylesheet" href="glow/css/superfish.css">
<link rel="stylesheet" href="glow/css/style.css">
<script src="glow/js/modernizr-2.6.2.min.js"></script>
<!-- 박윤수 추가하는부분(로그인창) -->
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script
	src="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet">
<!-- 박윤수 로그인창 종료 -->

<script src="glow/js/jquery.min.js"></script>
<script src="glow/js/jquery.easing.1.3.js"></script>
<script src="glow/js/bootstrap.min.js"></script>
<script src="glow/js/jquery.waypoints.min.js"></script>
<script src="glow/js/jquery.stellar.min.js"></script>
<script src="glow/js/hoverIntent.js"></script>
<script src="glow/js/superfish.js"></script>
<script src="glow/js/jquery.magnific-popup.min.js"></script>
<script src="glow/js/magnific-popup-options.js"></script>
<script src="glow/js/main.js"></script>

<style>
#login-dp {
	min-width: 250px;
	padding: 14px 14px 0;
	overflow: hidden;
	background-color: rgba(255, 255, 255, .8);
}

#login-dp .help-block {
	font-size: 12px
}

#login-dp .bottom {
	background-color: rgba(255, 255, 255, .8);
	border-top: 1px solid #ddd;
	clear: both;
	padding: 14px;
}

#login-dp .social-buttons {
	margin: 12px 0
}

#login-dp .social-buttons a {
	width: 49%;
}

#login-dp .form-group {
	margin-bottom: 10px;
}

.btn-fb {
	color: #fff;
	background-color: #3b5998;
}

.btn-fb:hover {
	color: #fff;
	background-color: #496ebc
}

.btn-tw {
	color: #fff;
	background-color: #55acee;
}

.btn-tw:hover {
	color: #fff;
	background-color: #59b5fa;
}

@media ( max-width :768px) {
	#login-dp {
		background-color: inherit;
		color: #fff;
	}
	#login-dp .bottom {
		background-color: inherit;
		border-top: 0 none;
	}
}
</style>



</head>
<body>
	<div id="fh5co-wrapper">
		<div id="fh5co-page">
			<div id="fh5co-header">
				<header id="fh5co-header-section">
					<div class="container">
						<div class="nav-header">
							<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
							<h1 id="fh5co-logo">
								<a href="index"></a><img src="logo.png" style="margin-left: -40px;">
							</h1>
							<!-- START #fh5co-menu-wrap -->
							<span style="position: absolute;">
								<nav id="fh5co-menu-wrap" role="navigation">
									<%-- <span style='position:absolute;right:0px'> --%>
									<ul class="sf-menu" id="fh5co-primary-menu">
										<Br>
										<li><a href="../makeHomepage/makeHomepageForm"
											class="fh5co-sub-ddown">ホームページ製作</a></li>
										<li><a href="../customerMenu/customerMenu"
											class="fh5co-sub-ddown">ホームページ管理</a></li>
										<li><a href="../board/QNABoardList">質問の掲示板</a></li>

										<%
											String email = (String) session.getAttribute("email");
											if (email == null) {
										%>
										<li class="dropdown">
										<li><a href="#" class="dropdown-toggle"
											data-toggle="dropdown">ログイン<span class="caret"></span></a>
											<ul id="login-dp" class="dropdown-menu" style="left:-40%;">
												<li>
													<div class="row">
														<div class="col-md-12">
															<form class="form" action="../client/login.action"
																method="post" id="list">
																<div class="form-group">
																	<label class="sr-only" for="exampleInputEmail2">アイディー</label>
																	<input type="text" id="email" name="email"
																		class="form-control" placeholder="アイディー" required>
																</div>
																<div class="form-group">
																	<label class="sr-only" for="exampleInputPassword2">비밀번호</label>
																	<input type="password" class="form-control"
																		id="password" name="password" placeholder="パスワード"
																		required>
																</div>
																<div class="form-group">
																	<input type="submit" class="btn btn-primary btn-block"
																		value="ログイン">
																</div>
																<div class="help-block text-right">
																	<a href="../client/passwordsearch_q.action"
																		style="color: gray">暗証番号を忘れました。</a>
																</div>

															</form>
														</div>
														<div class="bottom text-center">
															初めてですか。 <a href="../client/joinform.action"
																style="color: gray"><b>加入はここで</b></a>
														</div>
													</div>
												</li>
											</ul></li>
										<%
											} else {
										%>
										<li class="fh5co-sub-ddown">
										<li><a href="../client/modify_form"
											class="fh5co-sub-ddown">情報修正</a></li>
										<li><a href="../client/logout.action"
											class="fh5co-sub-ddown">ログアウト</a></li>
										</li>
										<li><a class="fh5co-sub-ddown">${session.name}さまいらっしゃいませ.</a></li>
										</li>
										<%
											}
										%>
									</ul>
									<%-- </span> --%>
								</nav>
							</span>
						</div>
					</div>
				</header>
			</div>
		</div>

		<div class="fh5co-hero">
			<div class="fh5co-overlay"></div>
			<div class="fh5co-cover text-center"
				data-stellar-background-ratio="0.5"
				style="background-image: url(glow/images/cover_bg_2.jpg);height:100%;">
				<div class="desc animate-box">
					<h2>誰でも簡単にホームページを作ることができます。</h2>
					<span>マウスやキーボードだけで作ることができます!</span>
				<!-- 	<p>
						<a class="btn btn-primary btn-lg btn-learn" href="#">使用法</a> <a
							class="btn btn-primary btn-lg popup-vimeo btn-video"
							href="https://vimeo.com/channels/staffpicks/93951774"><i
							class="icon-play"></i> Watch Video</a>
					</p> -->
				</div>
			</div>

		</div>
	</div>


</body>
</html>


