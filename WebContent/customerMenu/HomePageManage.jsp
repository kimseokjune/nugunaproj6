<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title></title>
<style>
#customerMenuleft {
	width: 20%;
	height: 100%;
	position: fixed;
}

#customerMenuright {
	width: 80%;
	height: auto;
	position: absolute;
	right: 0;
}
</style>
</head>
<body>

	<!--   메뉴시작  --------------------------------------------------------------------------------------->
	<div id="menu">
		<%@include file="../menu/menu.jsp"%>
	</div>
	<!--   메뉴종료   --------------------------------------------------------------------------------------->


	<!--   본문시작   --------------------------------------------------------------------------------------->
	<div id="content">


		<!--왼쪽메뉴-->
		<div id="customerMenuleft"><%@include
				file="../customerMenu/left.jsp"%>
		</div>

		<div id="customerMenuright">
			<div id="board_wrapper">
				<table class="table">
					<thead>
						<tr>
							<th>タイトル</th>
							<th>店の名前</th>
							<th>ドメイン</th>
							<th>残りの使用期間</th>

							<th>修正</th>
							<th>試し読み</th>
						</tr>
					</thead>
					<tbody>
						<s:iterator value="List" status="status">
							<tr>
								<td><s:property value="title" /></td>
								<td><s:property value="shopname" /></td>
								<td><s:property value="domain" /></td>
								<td><s:property value="hosting_day"/>日残りました。</td>
								<td><a
									href="../makeHomepage/edit?hosting_id=<s:property value="hosting_id"/>">修正</a></td>
								<td><a href="../makeHomepage/goHomepage?hosting_id=<s:property value="hosting_id"/>"　TARGET="_blank">試し読み</a></td>
							</tr>
						</s:iterator>
					</tbody>
				</table>
			</div>
		</div>

	</div>
	<!--   본문종료   --------------------------------------------------------------------------------------->


</body>
</html>