<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link href='http://fonts.googleapis.com/css?family=Quicksand:400,700'
	rel='stylesheet' type='text/css' />
<script type="text/javascript"
	src="http://code.jquery.com/jquery-2.0.0.min.js" /></script>

<link
	href="http://netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.min.css"
	rel="stylesheet" />
<link rel='stylesheet' href='../customerMenu/css/leftmenu.css'>
<script>
	jQuery(document).ready(
			function() {
				jQuery('ul.form li a').click(
						function(e) {
							e.preventDefault(); // prevent the default action
							e.stopPropagation; // stop the click from bubbling
							jQuery(this).closest('ul').find('.selected')
									.removeClass('selected');
							jQuery(this).parent().addClass('selected');
						});
			});
</script>


<ul class="form">


 <!--  선택된 느낌 주고싶으면  class="selected" 쓰면됨  -->

	<li class="selected"><a class="profile" href="#"><i
			class="icon-user"></i>ホスティング管理</a></li>
	<!-- <li><a class="messages" href="#"><i class="icon-envelope-alt"></i>결제정보</a></li> -->
	<!-- 		<li><a class="settings" href="#"><i class="icon-cog"></i>App Settings</a></li>
		<li><a class="logout" href="#"><i class="icon-signout"></i>Logout</a></li> -->
</ul>