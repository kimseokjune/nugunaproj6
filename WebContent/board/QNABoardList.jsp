<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>문의글 리스트 페이지</title>
<link rel='stylesheet' href='../board/css/board.css'>
<script>
function pagingFormSubmit(currentPage){
	//폼네임.인풋네임의 값
	document.pagingForm.currentPage.value = currentPage;
	document.pagingForm.submit();
}
</script>
</head>
<body>

	<!--   메뉴시작  -->
	<div id="menu">
		<%@include file="../menu/menu.jsp"%>
	</div>
	<!--   메뉴종료  -->

	<!--   본문시작  -->
	<div id="content">

		<div id="board_wrapper">
			<h1>質問の掲示板</h1>
			<table class="table">
				<tr>
					<th id="board_no">番号</th>
					<th id="board_title">題目</th>
					<th id="board_writer">作成者</th>
					<th id="board_hits">クリック数</th>
					<th id="board_date">作成日</th>
				</tr>
				<!--------------------------------------- -->
				<s:iterator value="boardlist" status="status">
					<tr>
						<td><s:property value="boardno" /></td>
						<td><s:url id="readurl" value="QNABoardView.action">
								<s:param name="boardno" value="boardno" />
							</s:url> <a href="${readurl}"><s:property value="title" /></a></td>
						<td><s:property value="name" /></td>
						<td><s:property value="hits" /></td>
						<td><s:property value="inputdate" /></td>
					</tr>
				</s:iterator>
				<!-- -------------------- -->
			</table>
			<a href="QNABoardWriteForm.action" class="myButton">文を作成</a>
			<!-- 페이징시작 -->
			<div class="bottom_form">
				<form name="pagingForm" action="QNABoardList.action">
					<s:hidden name="searchField" />
					<s:hidden name="searchText" />
					<s:hidden name="currentPage" />
					<a href="#" onClick="pagingFormSubmit(${pagenavi.currentPage - 1})">&lt;</a>
					<s:iterator var="counter" begin="pagenavi.startPageGroup"
						end="pagenavi.endPageGroup">
						<a
							href="javascript:pagingFormSubmit('<s:property value="#counter"/>')"
							<s:if test="#counter == pagenavi.currentPage"> class="select"</s:if>>
							<s:property value="#counter" />
						</a> &nbsp;
			</s:iterator>
					<a href="javascript:pagingFormSubmit(${pagenavi.currentPage + 1})">&gt;</a>

				</form>
			</div>
			<!-- 페이징종료 -->


			<!-- 검색시작 -->
			<div class="bottom_form">
				<form id="search" action="QNABoardList.action" method="post">
					<p class="board_search">
						<s:select name="searchField"
							list="#{'all':'全体','title':'タイトル','content':'内容','name':'作成者' }"></s:select>
						<s:textfield name="searchText" />
						<a href="#" onclick="document.getElementById('search').submit();">サーチ</a>


					</p>
				</form>
			</div>

			<!-- 검색종료 -->

		</div>
		<!--  end of board_wrapper -->


	</div>
	<!--   본문종료  -->


</body>
</html>