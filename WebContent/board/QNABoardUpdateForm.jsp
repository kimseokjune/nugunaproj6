<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel='stylesheet' href='../board/css/board.css'>
<script>
	function goupdate() {
		list.action = "QNABoardUpdate.action";
		list.submit();
	}
</script>
</head>
<body>



	<div id="menu">
		<%@include file="../menu/menu.jsp"%>
	</div>




	<div id="board_wrapper">

		<s:form id="list" method="post" theme="simple">
			<s:hidden name="boardno"></s:hidden>
			<table class="table">
				<tr>
					<th class="board_th_width">作成者</th>
					<td><s:property value="board.name" /> <s:hidden
							name="board.name"></s:hidden></td>
				</tr>
				<tr>
					<th class="board_th_width">タイトル</th>
					<td><s:textfield name="board.title"  class="table"></s:textfield></td>
				</tr>
				<tr>
					<th colspan="2">内容</th>
				</tr>
				<tr>
					<td colspan="2"><s:textarea name="board.content"  class="textareasize"></s:textarea>
					</td>
				</tr>
				<!-- <tr>
					<th>첨부파일</th>
					<td><input type="file" /></td>
				</tr> -->

			</table>
		</s:form>
		<p>

			<a href="QNABoardList.action" class="myButton">リスト</a> <a href="#"
				onclick="goupdate()" class="myButton">修正</a>
		</p>
	</div>
</body>
</html>