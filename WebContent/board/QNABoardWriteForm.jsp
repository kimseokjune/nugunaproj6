<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel='stylesheet' href='../board/css/board.css'>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
	function gowrite() {
		list.action = "QNABoardWrite.action";
		list.submit();
	}
</script>
</head>
<body>
	<!--   메뉴시작  -->
	<div id="menu">
		<%@include file="../menu/menu.jsp"%>
	</div>
	<!--   메뉴종료  -->
	<div id="content">

		<div id="board_wrapper">
			<form id="list" method="post">
				<table class="table">
					<tr>
						<th class="board_th_width">作成者</th>
						<td><s:property value="#session.name" /> <input
							type="hidden" name="board.name"
							value="<s:property value="#session.name"/>"> <input
							type="hidden" name="board.email"
							value="<s:property value="#session.loginid"/>" /></td>
					</tr>
					<tr>
						<th class="board_th_width">タイトル</th>
						<td><input id="board.title" name="board.title" type="text"  class="table" /></td>
					</tr>
					<tr>
						<th colspan="2" >内容</th>
					</tr>
					<tr>
						<td colspan="2"><textarea id="board.content" class="textareasize"
								name="board.content"></textarea></td>
					</tr>
					<!-- <tr>
					<th>첨부파일</th>
					<td><input type="file" /></td>
				</tr> -->

				</table>
			</form>

			<p>
				<a href="QNABoardList.action" class="myButton">キャンセル</a> <a href="#" onclick="gowrite()" class="myButton">登録</a>
			</p>
		</div>
	</div>

</body>
</html>