<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link rel='stylesheet' href='../menu/css/all.css'>
<!-- Bootstrap core CSS -->
<link href="../menu/assets/css/bootstrap.css" rel="stylesheet">
<!-- Custom styles for this template -->
 <link href="../menu/assets/css/style.css" rel="stylesheet"> 
<link href="../menu/assets/css/font-awesome.min.css" rel="stylesheet">


<div class="navbar-default navbar-fixed-top" role="navigation">
	<div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a href="../index/index
				"><img src="../index/logo75.png"></a>
		</div>
		<div class="navbar-collapse collapse navbar-right">
			<ul class="nav navbar-nav">

				<!--    공지사항 : 중간 발표때까지 잠시 숨김 -->
				<!-- 	<li><a href="index.html">공지사항</a></li> -->
				<!-- <li><a href="../index/howtouse">사용법</a></li> -->
				<li><a href="../makeHomepage/makeHomepageForm">ホームページ製作</a></li><!-- 홈페이지만들기 -->
				<li><a href="../customerMenu/customerMenu">ホームページ管理</a></li><!--홈페이지관리  -->
				<li><a href="../board/QNABoardList">質問の掲示板</a></li><!--질문 게시판  -->

				<!-- 로그인X -->
				<s:if test="#session.email == null">
					<li class="dropdown"><a href="../client/loginForm">ログイン</a>
				</s:if>

				<s:else>
					<!-- 로그인 -->
					<li></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown"> <s:property value="#session.name" />さまいらっしゃいませ！<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="../client/logout">ログアウト</a></li>
							<li><a href="../client/modify_form">情報修正</a></li><!--정보수정　  -->
						</ul></li>

				</s:else>
			</ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
 <script src="../menu/assets/js/bootstrap.min.js"></script>
<script src="../menu/assets/js/retina-1.1.0.js"></script>
<script src="../menu/assets/js/jquery.hoverdir.js"></script>
<script src="../menu/assets/js/jquery.hoverex.min.js"></script>
<%--  <script src="../menu/assets/js/jquery.prettyPhoto.js"></script>  --%>
<script src="../menu/assets/js/jquery.isotope.min.js"></script>
<script src="../menu/assets/js/custom.js"></script>

<%-- <div id="menuWrapper">
	<div id="left">
		<%@include file="../menu/left.jsp"%>
	</div>
	<div id="right">
		<%@include file="../menu/right.jsp"%>
	</div>
</div>
 --%>