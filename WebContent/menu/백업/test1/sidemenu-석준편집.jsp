<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!-- js  -->


<script src="../script/toolBoxScroll.js"></script>
<script src="../menu/js/sidemenu.js"></script>
<script src="../menu/js/widgetText.js"></script>
<script src="../menu/js/widgetPic.js"></script>
<script src="../menu/js/widgetBoard.js"></script>
<script src="../menu/js/widgetButton.js"></script>
<script src="../menu/js/BootSideMenu.js"></script>
<script src="../menu/mega-dropdown/js/modernizr.js"></script>


<!-- css -->


<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"> -->

<link rel="stylesheet" href="../menu/js/BootSideMenu.css"> 
 <link rel="stylesheet" href="../menu/mega-dropdown/css/reset.css">
<link rel="stylesheet" href="../menu/mega-dropdown/css/style.css">


<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>
<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXfkKycQcYx5X9_Y_9_WP1XpINVSElrcM&callback=initMap"></script>



<style type="text/css">
/* .user {
	margin-bottom: 5px;
}

.green {
	background: #3D9970;
	/*   font-size: 1em; */
	/*   border-bottom: 0.2em solid #398e68; */
	/*   font-weight: 700;
  letter-spacing: 3px;
  text-transform: uppercase; */
}

#videobcg {
	position: fixed;
	top: 0px;
	left: 0px;
	min-width: 100%;
	min-height: 100%;
	width: auto;
	height: auto;
	z-index: 0;
	overflow: hidden;
}*/
</style>

<script>
	$(function() {
		var owner = $("#owner").val();
		var templatename = $("#templatename").val();
		var version = $("#version").val();

		/* $("#homepageSaveMenus").append('<a href="#" class="btn btn-success btn-rounded" onclick="return view()">화면보기</a><a id="pageSave" onclick="pageSave()" class="btn btn-warning btn-rounded">저장하기</a>'); */
		/* $("#homepageSaveMenus").append('<a href="../makeHomepage/goHomepage?owner='+owner+'&templatename='+templatename+'&version='+version+'" class="btn btn-success btn-rounded" onclick="return view()">화면보기</a><a id="pageSave" onclick="pageSave()" class="btn btn-warning btn-rounded">저장하기</a>');
		 */
		/* $("#innercontent").append('<h1 id="homeowner">'+ owner + '의 홈페이지</h1>');
		$("#homeowner").draggable(); */
	});
	function pageSave() {
		/* $("#menuWrapper").hide(); */
		/* $("#homepageSaveMenu").hide(); */
		$("#tools").remove();
		/* $("#menu").hide();*/
		$('nav div div *').css("border", "0px dotted");
		$('header div div *').css("border", "0px dotted");
		$('section div div *').css("border", "0px dotted")
		$
				.ajax({
					url : 'htmlsave',
					type : 'post',
					data : {
						html : document.body.innerHTML,
						/* 	owner : $("#owner").val(), */
						templatename : $("#templatename").val(),
						/* 	version : $("#version").val(), */
						hosting_id : $("#hosting_id").val(),
						encording : '<&#37;&#64; page language=&#34;java&#34; contentType=&#34;text/html; charset=UTF-8&#34; pageEncoding=&#34;UTF-8&#34;%>'
					},
					success : function(data) {
						alert("저장완료하였습니다.");
						window.location.replace('edit.action?hosting_id='+ $("#hosting_id").val());
					}
				});


	}

	function view() {
	/* 	if (pageSaveflag == 0) {
			alert("저장하기를 눌러주세요!");
			return false;
		} else { */

			//미리보기는 새창으로뜸
			window.open("../makeHomepage/goHomepage?hosting_id="
					+ $("#hosting_id").val(), '_blank');
			/* 
			 location.href = '../makeHomepage/goHomepage?hosting_id='
			 + $("#hosting_id").val(); */
			return true;
	/* 	}*/
	}
</script>

<script type="text/javascript">
	/* 툴박스는 맨위에있어야 잘작동됩니다. 위치움직이지마세요  */
	$(document).ready(function() {

		$('#toolbox').BootSideMenu({
			side : "left",
			autoClose : false
		});
		var e;

		scrollControl();
		widgetPic();
		widgettext();
		widgetBoard();
		widgetButton();

		/* backgroundCH(e); */

	});
	/* 툴박스는 맨위에있어야 잘작동됩니다. 위치움직이지마세요  */
</script>
<script>
	$('#ownermaps').draggable({
		start : function(event, ui) {
			$(this).draggable("option", "revert", true);
		}
	});

	$('#ownermaps').on('dragstop', function(event) {
		$.post("../ownerContent/widgetMap/widgetMap.jsp", function(data) {
			$("#innercontent").append(data);

		});
		$("#mapsgo").draggable();
		$("#mapsgo").css({
			"position" : "fixed",
			"top" : event.clientY,
			"left" : event.clientX,
			"z-index" : "0"
		});
	});
</script>

	<div id="toolbox">

		<div class="cd-dropdown-wrapper">
			<div class="user">
				<h3 align="center">
					<a href="../index/index"><img src="../menu/side_logo.png"></a>
				</h3>
				<p>
				<h4 class="navbar-link" style="text-align: center;">누구나</h4>
				<br> <a href="#" class="btn btn-success btn-rounded"
					onclick="return view()" style="font-size: 9px;">미리보기</a><a
					id="pageSave" onclick="pageSave()"
					class="btn btn-warning btn-rounded" style="font-size: 9px;">저장하기</a>

			</div>
			<!-- /////////////////////////////////////////////////////// -->

			<div>


				<ul class="cd-dropdown-content">

					<li class="has-children"><a href="#" id="newtext">템플릿</a>

						<ul class="cd-dropdown-gallery is-hidden">
							<li class="go-back"><a href="#0">Menu</a></li>

							<li><a href="#" onclick="backgroundCH('basic')"
								class="cd-dropdown-item"><img
									src="../makeHomepage/template/basic/mini_basic.jpg"
									alt="기본 템플릿" style="padding-right: 5px;">기본템플릿</a></li>

							<li><a href="#"
								onclick="backgroundCH('startbootstrap-freelancer-gh-pages')"
								class="cd-dropdown-item"><img
									src="../makeHomepage/template/startbootstrap-freelancer-gh-pages/mini_freelancer.jpg"
									alt="freelaner" style="">freelaner_템플릿</a></li>

							<li><a href="#"
								onclick="backgroundCH('startbootstrap-grayscale-gh-pages')"
								class="cd-dropdown-item"><img
									src="../makeHomepage/template/startbootstrap-grayscale-gh-pages/mini_grayscale.jpg"
									alt="grayscale" style="padding-right: 5px;">grayscale_템플릿</a>
							</li>

							<li><a href="#"
								onclick="backgroundCH('startbootstrap-stylish-portfolio-gh-pages')"
								class="cd-dropdown-item"><img
									src="../makeHomepage/template/startbootstrap-stylish-portfolio-gh-pages/mini_stylish-portfolio.jpg"
									alt="stylish-portfolio" style="">stylish-portfolio_템플릿</a></li>
						</ul> <!-- .cd-dropdown-gallery --></li>
					<!-- .has-children -->


					<li class="has-children"><a href="#" id="newtext">동영상배경</a>

						<ul class="cd-dropdown-gallery is-hidden">
							<li class="go-back"><a href="#0">Menu</a></li>
							<!-- 	<li class="see-all"><a href="#">템플릿선택</a></li> -->
							<li><a href="#" onclick="backgroundCH('mv1')"
								class="cd-dropdown-item"><img
									src="https://i.vimeocdn.com/video/594701321_640x360.jpg"
									alt="강" style="padding-right: 5px;">동영상-강</a></li>

							<li><a href="#" onclick="backgroundCH('mv2')"
								class="cd-dropdown-item"><img
									src="https://i.vimeocdn.com/video/593633462_640x360.jpg"
									alt="d물고기" style="">동영상-물고기</a></li>

							<li><a href="#" onclick="backgroundCH('mv3')"
								class="cd-dropdown-item"><img
									src="https://i.vimeocdn.com/video/593166446_640x360.jpg"
									alt="수영장" style="padding-right: 5px;">동영상-수영장</a></li>

							<li><a href="#" onclick="backgroundCH('mv4')"
								class="cd-dropdown-item"><img
									src="https://i.vimeocdn.com/video/585147446_640x360.jpg"
									alt="stylish-portfolio" style="">동영상-비</a></li>
						</ul> <!-- .cd-dropdown-gallery --></li>
					<!-- .has-children -->









					<li class="has-children"><a href="#" id="newtext">미디어</a>
						<ul class="cd-dropdown-icons is-hidden">
							<li class="go-back"><a href="#0">Menu</a></li>

							<li id="AA"><a class="cd-dropdown-item item-1" href="#">
									<h3>사진위젯</h3>
									<p>자신만의 사진을 올려보세요</p>
							</a></li>

							<li id="BB"><a class="cd-dropdown-item item-2" href="#">
									<h3>동영상위젯</h3>
									<p>기억에 남는 동영상을 올려보세요</p>
							</a></li>
						</ul></li>

					<!-- //////////////////// 게시판 ///////////////////// -->
					<li class="has-children"><a href="#" id="newtext">게시판</a>
						<ul class="cd-dropdown-icons is-hidden">
							<li class="go-back"><a href="#0">Menu</a></li>
							<li class="see-all"><a href="http://codyhouse.co/?p=748">Browse
									Services</a></li>
							<li id="wbnoti"><a class="cd-dropdown-item notiboard"
								href="#">
									<h3>공지사항</h3>
									<p>자신만의 사진을 올려보세요</p>
							</a></li>

							<li id="wbline"><a class="cd-dropdown-item lineboard"
								href="#">
									<h3>한줄평</h3>
									<p>기억에 남는 동영상을 올려보세요</p>
							</a></li>
						</ul> <!-- .cd-dropdown-icons --></li>
					<!-- .has-children -->
					<!-- ////////////////////////////////////////////////////////// -->

					<li class="has-children"><a href="#" id="newtext">기타위젯</a>
						<ul class="cd-dropdown-icons is-hidden">
							<li class="go-back"><a href="#0">Menu</a></li>
							<li class="see-all"><a href="http://codyhouse.co/?p=748">Browse
									Services</a></li>
							<li id="ownermaps"><a class="cd-dropdown-item notiboard"
								href="#">
									<h3>지도위젯</h3>
									<p>가게의 위치를 보여주세요</p>
							</a></li>

							<li id="clocks"><a class="cd-dropdown-item lineboard"
								href="#">
									<h3>시계위젯</h3>
									<p>시간을 지켜보세요</p>
							</a></li>
						</ul> <!-- .cd-dropdown-icons --></li>
					<!-- .has-children -->











					<!-- <li class="has-children">
		<a href="#" id="newtext">xxx</a>
		
			<ul class="cd-dropdown-icons is-hidden">
				<li class="go-back"><a href="#0">Menu</a></li>
					<li class="see-all"></li>
						<li>
							<a class="cd-dropdown-item" href="#"  onclick="backgroundCH('mv1')">
							<img src="https://i.vimeocdn.com/video/594701321_640x360.jpg" width="100%" height="100%" alt="기본 템플릿"><h3>배경 - 강</h3>
							</a>
							
						</li>

						<li>
							<a class="cd-dropdown-item item-2" href="#">
								<h3>동영상위젯</h3>
								<p>기억에 남는 동영상을 올려보세요</p>
							</a>
						</li>
			</ul>
	</li> -->
					<!-- 
							<li class="has-children">
								<a href="http://codyhouse.co/?p=748">Bottoms</a>

								<ul class="is-hidden">
									<li class="go-back"><a href="#0">Clothing</a></li>
									<li class="see-all"><a href="http://codyhouse.co/?p=748">All Bottoms</a></li>
									<li><a href="http://codyhouse.co/?p=748">Casual Trousers</a></li>
									<li class="has-children">
										<a href="#0">Jeans</a>

										<ul class="is-hidden">
											<li class="go-back"><a href="#0">Bottoms</a></li>
											<li class="see-all"><a href="http://codyhouse.co/?p=748">All Jeans</a></li>
											<li><a href="http://codyhouse.co/?p=748">Ripped</a></li>
											<li><a href="http://codyhouse.co/?p=748">Skinny</a></li>
								
										</ul>
									</li>
									<li><a href="#0">Leggings</a></li>
									<li><a href="#0">Shorts</a></li>
								</ul>
							</li>

							<li class="has-children">
								<a href="http://codyhouse.co/?p=748">Jackets</a>

								<ul class="is-hidden">
									<li class="go-back"><a href="#0">Clothing</a></li>
									<li class="see-all"><a href="http://codyhouse.co/?p=748">All Jackets</a></li>
									<li><a href="http://codyhouse.co/?p=748">Blazers</a></li>
									<li><a href="http://codyhouse.co/?p=748">Bomber jackets</a></li>

								</ul>
							</li>

							<li class="has-children">
								<a href="http://codyhouse.co/?p=748">Tops</a>

								<ul class="is-hidden">
									<li class="go-back"><a href="#0">Clothing</a></li>
									<li class="see-all"><a href="http://codyhouse.co/?p=748">All Tops</a></li>
									<li><a href="http://codyhouse.co/?p=748">Cardigans</a></li>
									<li><a href="http://codyhouse.co/?p=748">Coats</a></li>
								
									<li class="has-children">
										<a href="#0">T-Shirts</a>

										<ul class="is-hidden">
											<li class="go-back"><a href="#0">Tops</a></li>
											<li class="see-all"><a href="http://codyhouse.co/?p=748">All T-shirts</a></li>
											<li><a href="http://codyhouse.co/?p=748">Plain</a></li>
											<li><a href="http://codyhouse.co/?p=748">Print</a></li>
		
										</ul>
									</li>
									<li><a href="http://codyhouse.co/?p=748">Vests</a></li>
								</ul>
							</li>
						</ul> .cd-secondary-dropdown
					</li> .has-children -->


					<!-- 					<li class="cd-divider">Divider</li>

					<li><a href="http://codyhouse.co/?p=748">Page 1</a></li>
					<li><a href="http://codyhouse.co/?p=748">Page 2</a></li>
					<li><a href="http://codyhouse.co/?p=748">Page 3</a></li> -->
				</ul>
				<!-- .cd-dropdown-content -->
			</div>
		</div>
	</div>

	<!-- 	<main class="cd-main-content">
		your content here
	</main>
	 -->

	<script src="../menu/mega-dropdown/js/jquery.menu-aim.js"></script>
	<!-- menu aim -->
	<script src="../menu/mega-dropdown/js/main.js"></script>
	<!-- Resource jQuery -->
