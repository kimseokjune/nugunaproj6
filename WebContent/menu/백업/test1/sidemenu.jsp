<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
<script src="../script/toolBoxScroll.js"></script>
	
<link rel="stylesheet" href="../menu/mega-dropdown/css/bootstrap.css">

<script src="../menu/js/sidemenu.js"></script>
<script src="../menu/js/widgetText.js"></script>
<script src="../menu/js/widgetPic.js"></script>
<script src="../menu/js/widgetBoard.js"></script>
<script src="../menu/js/widgetButton.js"></script>

<link rel="stylesheet" href="../menu/js/BootSideMenu.css">

<script src="../menu/js/BootSideMenu.js"></script>

<script src="../menu/mega-dropdown/js/modernizr.js"></script> <!-- Modernizr -->

<script src="../menu/js/evol-colorpicker.min.js"></script>
<script src="../menu/js/jquery.fontselect.js"></script>
<link rel="stylesheet" href="../menu/js/fontselect.css">
<link rel="stylesheet" href="../menu/js/evol-colorpicker.min.css">


<link rel="stylesheet" href="../menu/mega-dropdown/css/reset.css"> <!-- CSS reset -->
<link rel="stylesheet" href="../menu/mega-dropdown/css/style.css"> <!-- Resource style -->

  	<link rel="stylesheet" href="../ownerContent/css/jquery.syaku.modal.css">
<script src="../ownerContent/js/jquery.syaku.modal.js"></script>


<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXfkKycQcYx5X9_Y_9_WP1XpINVSElrcM&callback=initMap"></script>
<!-- ////// 시계 css, js 시작 //// -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">




<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js'></script>
<!-- <script src="js/index.js"></script> -->


<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/component.css" />

 
 <style type="text/css">
  	.user {
  	
	margin-bottom: 5px;
	}
	
	.green {
  background: #3D9970;
/*   font-size: 1em; */
/*   border-bottom: 0.2em solid #398e68; */
/*   font-weight: 700;
  letter-spacing: 3px;
  text-transform: uppercase; */
}
/* #videobcg { 
     position: fixed;
     top: 0px;
     left: 0px;
     min-width: 100%;
     min-height: 100%;
     width: auto;
     height: auto;
     z-index: 1;
     overflow: hidden;
} */
video{
	position:absolute;
	top: 50%;
	left:50%;
	transform: translate(-50%, -50%);
	min-width:100%;
	min-height:100%;
	width:auto;
	height:auto;
	z-index:0;
}
custom-handle {
    width: 3em;
    height: 1.6em;
    top: 50%;
    margin-top: -.8em;
    text-align: center;
    line-height: 1.6em;
  }
   #custom-handle2 {
    width: 3em;
    height: 1.6em;
    top: 50%;
    margin-top: -.8em;
    text-align: center;
    line-height: 1.6em;
  }
  .p_center{ text-align : center;}


.image-container{width: 200px;height:200px;position: absolute;} 
.text-container{width: 200px;height:200px;position: absolute;} s
.textBtn{width:30px; height:30px; content: url(../menu/img/icon_text.png);}
.imageBtn{width:30px; height:30px; content: url(../menu/img/icon_photo.png);}
.handleBtn{width:30px; height:30px; content: url(../menu/img/icon_cog.png);}
.deleteBtn{width:30px; height:30px; content: url(../menu/img/icon_rx.png); align:right;}
.deleteBtn2{width:30px; height:30px; content: url(../menu/img/icon_x.png); align:right;}

 </style> 	
	<title>Mega Dropdown | CodyHouse</title>
</head>
<script>
$(function(){
	var owner = $("#owner").val();
	var templatename = $("#templatename").val();
	var version = $("#version").val();
	
	/* $("#homepageSaveMenus").append('<a href="#" class="btn btn-success btn-rounded" onclick="return view()">화면보기</a><a id="pageSave" onclick="pageSave()" class="btn btn-warning btn-rounded">저장하기</a>'); */
	/* $("#homepageSaveMenus").append('<a href="../makeHomepage/goHomepage?owner='+owner+'&templatename='+templatename+'&version='+version+'" class="btn btn-success btn-rounded" onclick="return view()">화면보기</a><a id="pageSave" onclick="pageSave()" class="btn btn-warning btn-rounded">저장하기</a>');
	 */
	/* $("#innercontent").append('<h1 id="homeowner">'+ owner + '의 홈페이지</h1>');
	$("#homeowner").draggable(); */
});
function pageSave() {
	/* $("#menuWrapper").hide(); */
	/* $("#homepageSaveMenu").hide(); */
	$("#tools").remove();
	/* $("#menu").hide();*/
	$('nav div div *').css("border", "0px dotted");
	$('header div div *').css("border", "0px dotted");
	$('section div div *').css("border", "0px dotted")
	$
			.ajax({
				url : 'htmlsave',
				type : 'post',
				data : {
					html : document.body.innerHTML,
					/* 	owner : $("#owner").val(), */
					templatename : $("#templatename").val(),
					/* 	version : $("#version").val(), */
					hosting_id : $("#hosting_id").val(),
					encording : '<&#37;&#64; page language=&#34;java&#34; contentType=&#34;text/html; charset=UTF-8&#34; pageEncoding=&#34;UTF-8&#34;%>'
				},
				success : function(data) {
					alert("저장완료하였습니다.");
					window.location.replace('edit.action?hosting_id='+ $("#hosting_id").val());
				}
			});


}

function view() {
/* 	if (pageSaveflag == 0) {
		alert("저장하기를 눌러주세요!");
		return false;
	} else { */

		//미리보기는 새창으로뜸
		window.open("../makeHomepage/goHomepage?hosting_id="
				+ $("#hosting_id").val(), '_blank');
		/* 
		 location.href = '../makeHomepage/goHomepage?hosting_id='
		 + $("#hosting_id").val(); */
		return true;
/* 	}*/
}
</script>

<script type="text/javascript">
/* 툴박스는 맨위에있어야 잘작동됩니다. 위치움직이지마세요  */
$(document).ready(function() {

		$('#toolbox').BootSideMenu({
			side : "left",
			autoClose : false
		});
		$('#toolbox').css('background-color','rgba(255,255,255,0.0)');
		
		var e;
		
		scrollControl(); 
		widgetPic();
		widgettext();
		
		widgetBoard();
		
		widgetButton();
		
		backgroundCH(e);
			
});
/* 툴박스는 맨위에있어야 잘작동됩니다. 위치움직이지마세요  */
</script>
<script>
$('#ownermaps').draggable({
	start : function(event, ui) {
		$(this).draggable("option", "revert", true);
	}
});

$('#ownermaps').on('dragstop', function(event) {
	$.post("../ownerContent/widgetMap/widgetMap.jsp", function(data) {
		$("#innercontent").append(data);

			   });
	$("#mapsgo").draggable(); 
	$("#mapsgo").css({
"position" : "fixed",
"top" : event.clientY,
"left" : event.clientX,
"z-index" : "0"
	});
});


$(function(){
	

$('#clocks').draggable({
	start : function(event, ui) {
		$(this).draggable("option", "revert", true);
	}
});

$('#clocks').on('dragstop', function(event) {
	$.post("../ownerContent/widgetClock/widgetclock.html", function(data) {
		$("#innercontent").append(data);

			   });
	$("#clock").draggable(); 
	$("#clock").css({
"position" : "absolute",
"top" : event.clientY,
"left" : event.clientX,
"z-index" : "0"
	});
});

});




//재로드시 드래거블 다시 달아주기
$('document').ready(function(){
      //사진
      //버튼이 중복추가되는문제 >> 선삭제 후추가
      $('div[name=AAA] input').remove();
      $('div[name=AAA] label').remove();
      $('div[name=AAA]').draggable();
      $('div[name=AAA]').each(function(key, el) {
                     //각각의 엘레멘트에 ID추가
                     $(el).attr('id', 'AAA_' + key);
                     $('#AAA_'+key).resizable();
                     //이미지변경/삭제 버튼이 든 div 추가 (안보이게)
                     $(el).append(
                                 '<div name="AAA_option" class="div_option">'
                                       + '<label for="imageUp_'+key+'"><img class="imageBtn"></label><input id="imageUp_'+key+'"type="file">'
                                       + '<label for="deleteDiv_'+key+'"><img class="deleteBtn"></label><input id="deleteDiv_'+key+'" type="button" value="삭제">'
                                       + '</div>');
                     $('.div_option').css('display','none');
                     $('.div_option').children('input').css('display', 'none');
                     //각 엘레멘트별 이미지변환 타겟을 지정함 
                     $("#imageUp_" + key).on('change',function() {
                                    if (typeof (FileReader) != "undefined") {
                                       var reader = new FileReader();
                                       reader.onload = function(e) {
                                          $('#AAA_'+key+ ' img:first').attr('src',e.target.result);
                                       }
                                       reader.readAsDataURL($(this)[0].files[0]);
                                    } else {
                                       alert("This browser does not support FileReader.");
                                    }
                                 });
                     //각 엘레멘트별 삭제
                     $("#deleteDiv_" + key).on('click',function() {
                              $('#AAA_' + key).remove();
                     })
                  });
      //마우스오버시 설정 div 보이기
      $('div[name=AAA]').on('mouseover', function(event) {
         $(this).children('div').css('display', 'inline');
      });
      $('div[name=AAA]').on('mouseleave', function(event) {
         $(this).children('div').css('display', 'none');
      });
      
      //Text
      
         //버튼이 중복추가되는문제 >> 선삭제 후추가
         $('div[name=BBB] input').remove();
         $('div[name=BBB] label').remove();
         $('div[name=BBB]').draggable();
         $('div[name=BBB]').each(function(key,el){
            //각각의 엘레멘트에 ID추가
            $(el).attr('id','BBB_'+key);
            //이미지변경/삭제 버튼이 든 div 추가 (안보이게)
            $(el).append('<div name="BBB_option" class="div_option">'+
                  /* '<label for="writeText_'+key+'"><img class="textBtn"></label><input id="writeText_'+key+'"type="button" value="수정">'+ */
                  '<label for="handleText_'+key+'"><img class="handleBtn"></label><input id="handleText_'+key+'" type="button" value="핸들링">'+
                  '<label for="deleteText_'+key+'"><img class="deleteBtn"></label><input id="deleteText_'+key+'" type="button" value="삭제">'+
                  '</div>');
            
            $('.div_option').css('display','none');
            $('.div_option').children('input').css('display','none');
            //텍스트변경
            /* var option = {trigger : $("#writeText_"+key),action : "click",type:"textarea",width:"200px"}; */
               var option = {action : "click",type:"textarea",width:"200px"};
            $('#BBB_'+key).children('p').editable(option,function(){});
            //핸들러 달기
            $('#handleText_'+key).on('click',function(event){
               $('.handler').remove();
               /* $('#innercontent').append('<div class="handler"><select class="handle_size"><option>1</option></select><input class="handle_color" type="color"></div>'); */
               $('#innercontent').append('<div id="temp_handler" class="handler">'+
                     '<p class="p_right"><label for="deleteBtn2"><img class="deleteBtn2"></label><input id="deleteBtn2" type="button" value="끄기"></p>'+
                     '<p class="p_center">Edit Your Text</p>'+
                     '<p class="p_center">font size</p><div id="slider"> <div id="custom-handle" class="ui-slider-handle"></div></div>'+
                     '<p class="p_center">font select</p><input class="handle_font" type="text">'+
                     '<p class="p_center">text color</p><input id="handle_color" value="#0000ffff" />'+
                     '<p class="p_center">background color</p><input id="handle_background" value="#0000ffff" />'+
                     '<p class="p_center">Opacity</p><div id="slider2"> <div id="custom-handle2" class="ui-slider-handle"></div></div>'+
                     '</div>');
               $('#deleteBtn2').css('display','none');
               //핸들러 위치설정
               $('#temp_handler').css({
                  "position" : "absolute",
                  "top" : event.pageY-100,
                  "left" : event.pageX-300,
                  "opacity" : 0.85,
                  "background-image" : "url('../menu/img/rectangle.png')",
                  "background-repeat" : "no-repeat",
                  "background-position" : "center",
                  "width" : "210px",
                  "height" : "390px",
                  "object-fit":"contain",
                  "z-index" : 9999999
               });
               $('.handler').draggable();
               //삭제버튼2 
               $('#deleteBtn2').on('click',function(){
                  $('.handler').remove();
               });
               //글자크기변경 슬라이더
               var handle = $( "#custom-handle" );
                $( "#slider" ).slider({
                   value : 10,
                   create: function() {handle.text( $( this ).slider( "value" )+"pt" );},
                    slide: function( event, ui ) {
                       handle.text( ui.value+"pt" );
                       $('#BBB_'+key).children('p').css('font-size',ui.value);
                    },
                    min : 6, max : 96
                });
                //글꼴 변경
               $('.handle_font').fontselect().change(function(){
                  var font = $(this).val().replace(/\+/g,' ');
                  font = font.split(':');
                  $('#BBB_'+key).children('p').css('font-family', font[0]);
               });
               //글자색변경
               $('#handle_color').colorpicker({transparentColor: true});
               $('#handle_color').on('change',function(){
                  $('#BBB_'+key).children('p').css('color',$(this).val());
                  if($(this).val() == '#0000ffff'){
                     $('#BBB_'+key).children('p').css('color','rgba(255,255,255,0)');
                     }   
               });
               //배경색변경
               $('#handle_background').colorpicker({transparentColor: true});
               $('#handle_background').on('change',function(){
                  $('#BBB_'+key).children('p').css('background-color',$(this).val());
                  if($(this).val() == '#0000ffff'){
                  $('#BBB_'+key).children('p').css('background-color','rgba(255,255,255,0)');
                  }
               });
               //배경색 투명도 슬라이더
               var handle2 = $( "#custom-handle2" );
                $( "#slider2" ).slider({
                   value : 100,
                   create: function() {handle2.text( $( this ).slider( "value" )+"%" );},
                    slide: function( event, ui ) {
                       handle2.text( ui.value+"%");
                       $('#BBB_'+key).children('p').css('opacity',ui.value/100);
                    },
                    min:0,max:100
                });
            });
            //각 엘레멘트별 삭제
            $("#deleteText_"+key).on('click',function(){
               $('#BBB_'+key).remove();
            })
         });
         //마우스오버시 설정 div 보이기
         $('div[name=BBB]').on('mouseover',function(event){
            $(this).children('div').css('display','inline');
         });
         $('div[name=BBB]').on('mouseleave',function(event){
            $(this).children('div').css('display','none');
         });
   });




</script>

<body>
<div id="toolbox">
		
<div class="cd-dropdown-wrapper">
			<div class="user" >
		     		<h3 align="center"><a href="../index/index"><img src="../menu/side_logo.png"></a></h3><p>
     		 <h4 class="navbar-link" style="text-align: center;"><a href="../index/index">메인으로</a></h4><br>
     		 <a href="#" class="btn btn-success btn-rounded" onclick="return view()" style="font-size:16px; margin-left: 12px; width: 80px;">보기</a><a id="pageSave" onclick="pageSave()" class="btn btn-warning btn-rounded" style="font-size:16px; margin-left: 12px; width: 80px;">수정</a>
		
			</div>
	<!-- /////////////////////////////////////////////////////// -->
	
	<div>
	
	
<ul class="cd-dropdown-content">

	<li class="has-children">
						<a href="#" id="newtext"><button  class="btn btn-3 btn-3b icon-star-2">템플릿</button></a>

			<ul class="cd-dropdown-gallery is-hidden">
				<li class="go-back"><a href="#0">Menu</a></li>
					
						<li>
							<a href="#" onclick="backgroundCH('basic')" class="cd-dropdown-item" ><img src="../makeHomepage/template/basic/mini_basic.jpg" alt="기본 템플릿" style="center;padding-right:5px;">기본템플릿</a>
						</li>

						<li>
							<a href="#" onclick="backgroundCH('startbootstrap-freelancer-gh-pages')" class="cd-dropdown-item"><img src="../makeHomepage/template/startbootstrap-freelancer-gh-pages/mini_freelancer.jpg" alt="freelaner" style="center">freelaner_템플릿</a>
						</li>
						
						<li>
							<a href="#" onclick="backgroundCH('startbootstrap-grayscale-gh-pages')" class="cd-dropdown-item"><img src="../makeHomepage/template/startbootstrap-grayscale-gh-pages/mini_grayscale.jpg" alt="grayscale" style="center;padding-right:5px;">grayscale_템플릿</a>
						</li>

						<li>
							<a href="#" onclick="backgroundCH('startbootstrap-stylish-portfolio-gh-pages')" class="cd-dropdown-item"><img src="../makeHomepage/template/startbootstrap-stylish-portfolio-gh-pages/mini_stylish-portfolio.jpg" alt="stylish-portfolio" style="center">stylish-portfolio_템플릿</a>
						</li>
			</ul> <!-- .cd-dropdown-gallery -->
									
	</li> <!-- .has-children -->


<li class="has-children">
						<a href="#" id="newtext">동영상배경</a>

			<ul class="cd-dropdown-gallery is-hidden">
				<li class="go-back"><a href="#0">Menu</a></li>
				<!-- 	<li class="see-all"><a href="#">템플릿선택</a></li> -->
						<li>
							<a href="#" onclick="backgroundCH('mv1')" class="cd-dropdown-item"><img src="https://i.vimeocdn.com/video/594701321_640x360.jpg" alt="강" style="center;padding-right:5px;">동영상-강</a>
						</li>

						<li>
							<a href="#" onclick="backgroundCH('mv2')" class="cd-dropdown-item"><img src="https://i.vimeocdn.com/video/593633462_640x360.jpg" alt="d물고기" style="center;">동영상-물고기</a>
						</li>
						
						<li>
							<a href="#" onclick="backgroundCH('mv3')" class="cd-dropdown-item"><img src="https://i.vimeocdn.com/video/593166446_640x360.jpg" alt="수영장" style="center;padding-right:5px;">동영상-수영장</a>
						</li>

						<li>
							<a href="#" onclick="backgroundCH('mv4')" class="cd-dropdown-item"><img src="https://i.vimeocdn.com/video/585147446_640x360.jpg" alt="stylish-portfolio" style="center">동영상-비</a>
						</li>
			</ul> <!-- .cd-dropdown-gallery -->
									
	</li> <!-- .has-children -->






	
	
	
		<li class="has-children">
		<a href="#" id="newtext">미디어</a>
			<ul class="cd-dropdown-icons is-hidden">
				<li class="go-back"><a href="#0">Menu</a></li>
					
						<li id="AA">
							<a class="cd-dropdown-item item-1" href="#" style="font-size: 20px; font-weight: bold; color: green;">
							사진위젯
							<p>자신만의 사진을 올려보세요</p>
							</a>
						</li>

						<li id="BB">
							<a class="cd-dropdown-item item-2" href="#" style="font-size: 20px; font-weight: bold; color: green;">
								동영상위젯
								<p>기억에 남는 동영상을 올려보세요</p>
							</a>
						</li>
			</ul>
	</li>	
	
<!-- //////////////////// 게시판 ///////////////////// -->
				<li class="has-children">
						<a href="#" id="newtext">게시판</a>
						<ul class="cd-dropdown-icons is-hidden">
							<li class="go-back"><a href="#0">Menu</a></li>
							<li class="see-all"><a href="http://codyhouse.co/?p=748">Browse Services</a></li>
							<li id="wbnoti">
						<a class="cd-dropdown-item notiboard" href="#" style="font-size: 20px; font-weight: bold; color: green;">
								공지사항
								<p>자신만의 사진을 올려보세요</p>
								</a>
							</li>

							<li id="wbline">
								<a class="cd-dropdown-item lineboard" href="#" style="font-size: 20px; font-weight: bold; color: green;">
									한줄평
									<p>기억에 남는 동영상을 올려보세요</p>
								</a>
							</li>
						</ul> <!-- .cd-dropdown-icons -->
		</li> <!-- .has-children -->
<!-- ////////////////////////////////////////////////////////// -->

	<li class="has-children">
						<a href="#" id="newtext">기타위젯</a>
						<ul class="cd-dropdown-icons is-hidden">
							<li class="go-back"><a href="#0">Menu</a></li>
							<li class="see-all"><a href="http://codyhouse.co/?p=748">Browse Services</a></li>
							<li id="ownermaps">
						<a class="cd-dropdown-item notiboard" href="#" style="font-size: 20px; font-weight: bold; color: green;">
								지도위젯
								<p>가게의 위치를 보여주세요</p>
								</a>
							</li>

							<li id="clocks">
								<a class="cd-dropdown-item lineboard" href="#" style="font-size: 20px; font-weight: bold; color: green;">
									시계위젯
									<p>시간을 지켜보세요</p>
								</a>
							</li>
						</ul> <!-- .cd-dropdown-icons -->
		</li> <!-- .has-children -->











<!-- <li class="has-children">
		<a href="#" id="newtext">xxx</a>
		
			<ul class="cd-dropdown-icons is-hidden">
				<li class="go-back"><a href="#0">Menu</a></li>
					<li class="see-all"></li>
						<li>
							<a class="cd-dropdown-item" href="#"  onclick="backgroundCH('mv1')">
							<img src="https://i.vimeocdn.com/video/594701321_640x360.jpg" width="100%" height="100%" alt="기본 템플릿"><h3>배경 - 강</h3>
							</a>
							
						</li>

						<li>
							<a class="cd-dropdown-item item-2" href="#">
								<h3>동영상위젯</h3>
								<p>기억에 남는 동영상을 올려보세요</p>
							</a>
						</li>
			</ul>
	</li> -->
<!-- 
							<li class="has-children">
								<a href="http://codyhouse.co/?p=748">Bottoms</a>

								<ul class="is-hidden">
									<li class="go-back"><a href="#0">Clothing</a></li>
									<li class="see-all"><a href="http://codyhouse.co/?p=748">All Bottoms</a></li>
									<li><a href="http://codyhouse.co/?p=748">Casual Trousers</a></li>
									<li class="has-children">
										<a href="#0">Jeans</a>

										<ul class="is-hidden">
											<li class="go-back"><a href="#0">Bottoms</a></li>
											<li class="see-all"><a href="http://codyhouse.co/?p=748">All Jeans</a></li>
											<li><a href="http://codyhouse.co/?p=748">Ripped</a></li>
											<li><a href="http://codyhouse.co/?p=748">Skinny</a></li>
								
										</ul>
									</li>
									<li><a href="#0">Leggings</a></li>
									<li><a href="#0">Shorts</a></li>
								</ul>
							</li>

							<li class="has-children">
								<a href="http://codyhouse.co/?p=748">Jackets</a>

								<ul class="is-hidden">
									<li class="go-back"><a href="#0">Clothing</a></li>
									<li class="see-all"><a href="http://codyhouse.co/?p=748">All Jackets</a></li>
									<li><a href="http://codyhouse.co/?p=748">Blazers</a></li>
									<li><a href="http://codyhouse.co/?p=748">Bomber jackets</a></li>

								</ul>
							</li>

							<li class="has-children">
								<a href="http://codyhouse.co/?p=748">Tops</a>

								<ul class="is-hidden">
									<li class="go-back"><a href="#0">Clothing</a></li>
									<li class="see-all"><a href="http://codyhouse.co/?p=748">All Tops</a></li>
									<li><a href="http://codyhouse.co/?p=748">Cardigans</a></li>
									<li><a href="http://codyhouse.co/?p=748">Coats</a></li>
								
									<li class="has-children">
										<a href="#0">T-Shirts</a>

										<ul class="is-hidden">
											<li class="go-back"><a href="#0">Tops</a></li>
											<li class="see-all"><a href="http://codyhouse.co/?p=748">All T-shirts</a></li>
											<li><a href="http://codyhouse.co/?p=748">Plain</a></li>
											<li><a href="http://codyhouse.co/?p=748">Print</a></li>
		
										</ul>
									</li>
									<li><a href="http://codyhouse.co/?p=748">Vests</a></li>
								</ul>
							</li>
						</ul> .cd-secondary-dropdown
					</li> .has-children -->

				
<!-- 					<li class="cd-divider">Divider</li>

					<li><a href="http://codyhouse.co/?p=748">Page 1</a></li>
					<li><a href="http://codyhouse.co/?p=748">Page 2</a></li>
					<li><a href="http://codyhouse.co/?p=748">Page 3</a></li> -->
				</ul> <!-- .cd-dropdown-content -->
				</div>
</div>
	</div>

<!-- 	<main class="cd-main-content">
		your content here
	</main>
	 -->

<script src="../menu/mega-dropdown/js/jquery.menu-aim.js"></script> <!-- menu aim -->
<script src="../menu/mega-dropdown/js/main.js"></script> <!-- Resource jQuery -->
</body>
</html>