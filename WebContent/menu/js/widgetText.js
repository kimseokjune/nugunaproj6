/////////////////////////텍스트위젯/////////////////////
function widgettext() {
   $('#BB').draggable();         
   $('#BB').on('dragstop', function(event) {
      //버튼이 중복추가되는문제 >> 선삭제 후추가
      $('div[name=BBB] input').remove();
      $('div[name=BBB] label').remove();
      //img를 포함한 div를 append
      $('#innercontent').append('<div id="temp" name="BBB" class="text-container"><div id="text-field">Input Text!</div></div>');
      $('div[name=BBB]').draggable({});
      //$('div[name=BBB]').resizable({});
      //$('#temp').resizable();
      //첫 위치값 설정(임시아이디를 주고 설정?)
      $('#temp').css({
            "position" : "absolute",
            "top" : event.pageY,
            "left" : event.pageX,
            "z-index" : "5",
            "width" : "auto",
               "height" : "auto"
         });
      //텍스트변경
      /*var option = {trigger : $("#writeText_"+key),action : "click",type:"textarea",width:"200px"};*/
      
      var option = {action : "dblclick"};
      $('#temp').children('div:first').editable(option,function(){});
      $('div[name=BBB]').each(function(key,el){
         //각각의 엘레멘트에 ID추가
         $(el).attr('id','BBB_'+key);
         //이미지변경/삭제 버튼이 든 div 추가 (안보이게)
         $(el).append('<div name="BBB_option" class="div_option">'+
               '<label for="handleText_'+key+'"><img class="handleBtn"></label><input id="handleText_'+key+'" type="button" value="핸들링">'+
               '<label for="deleteText_'+key+'"><img class="deleteBtn"></label><input id="deleteText_'+key+'" type="button" value="삭제">'+
               '</div>');
         //$('.div_option').css('display','none');
         $('.div_option').children('input').css('display','none');
         //핸들러 달기
         $('#handleText_'+key).on('click',function(event){
            $('.handler').remove();
            
            /* $('#innercontent').append('<div class="handler"><select class="handle_size"><option>1</option></select><input class="handle_color" type="color"></div>'); */
            $('#innercontent').append('<div id="handler_wrapper" padding="10px" height="auto"><div id="temp_handler" class="handler" style="border-radius: 10px; filter:alpha(opacity:5); padding="10px">'+
                  '<p class="p_right"><input id="deleteBtn2" type="button" value="終了" class="btn btn-primary btn-block"></p>'+
                  '<p class="p_center">Edit Your Text</p>'+
                  '<p class="p_center">font size</p><div id="slider"> <div id="custom-handle" class="ui-slider-handle"></div></div>'+
                  '<p class="p_center">font select</p><input class="handle_font" type="text">'+
                  '<p class="p_center">text color</p><input id="handle_color" value="#0000ffff" />'+
                  '<p class="p_center">background color</p><input id="handle_background" value="#0000ffff" />'+
                  '<p class="p_center">Opacity</p><div id="slider2"> <div id="custom-handle2" class="ui-slider-handle"></div></div>'+
                  '</div></div>');
            //핸들러 위치설정
            $('#temp_handler').css({
               "position" : "absolute",
               "top" : event.pageY-100,
               "left" : event.pageX-300,
               "opacity" : 0.85,
               "background-image" : "url('../menu/img/rectangle.png')",
               "background-repeat" : "no-repeat",
               "background-position" : "center",
               "width" : "240px",
               "height" : "400px",
               "object-fit":"contain",
               "z-index" : 9999999
            });
            $('.handler').draggable();
            //글자크기변경 슬라이더
            var handle = $( "#custom-handle" );
             $( "#slider" ).slider({
                value : 10,
                create: function() {handle.text( $( this ).slider( "value" )+"pt" );},
                 slide: function( event, ui ) {
                    handle.text( ui.value+"pt" );
                    $('#BBB_'+key).children('div:first').css('font-size',ui.value);
                 },
                 min : 6, max : 96,
                 width : "220px"
             });
             //글꼴 변경
            $('.handle_font').fontselect().change(function(){
               var font = $(this).val().replace(/\+/g,' ');
               font = font.split(':');
               $('#BBB_'+key).children('div:first').css('font-family', font[0]);
            });
            //글자색변경
            $('#handle_color').colorpicker({transparentColor: true});
            $('#handle_color').on('change',function(){
               $('#BBB_'+key).children('div:first').css('color',$(this).val());
               if($(this).val() == '#0000ffff'){
                  $('#BBB_'+key).children('div:first').css('color','rgba(255,255,255,0)');
                  }   
            });
            //배경색변경
            $('#handle_background').colorpicker({transparentColor: true});
            $('#handle_background').on('change',function(){
               $('#BBB_'+key).children('div:first').css('background-color',$(this).val());
               if($(this).val() == '#0000ffff'){
               $('#BBB_'+key).children('div:first').css('background-color','rgba(255,255,255,0)');
               }
            });
            //배경색 투명도 슬라이더
            var handle2 = $( "#custom-handle2" );
             $( "#slider2" ).slider({
                value : 100,
                create: function() {handle2.text( $( this ).slider( "value" )+"%" );},
                 slide: function( event, ui ) {
                    handle2.text( ui.value+"%");
                    $('#BBB_'+key).children('div:first').css('opacity',ui.value/100);
                 },
                 min:0,max:100
             });
           //핸들러 창 삭제
               //$('#deleteBtn2').hide();
               $('#deleteBtn2').on('click',function(){
                  $('.handler').remove();
               });
         });
         //각 엘레멘트별 삭제
         $("#deleteText_"+key).on('click',function(){
            $('#BBB_'+key).remove();
         })
      });
      //마우스오버시 설정 div 보이기
      $('div[name=BBB]').on('mouseover',function(event){
         $(this).children('.div_option').show("fast");
      });
      $('div[name=BBB]').on('mouseleave',function(event){
         $(this).children('.div_option').delay(500).hide("slow");
      });
});
}