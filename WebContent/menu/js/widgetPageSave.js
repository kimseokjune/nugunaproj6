function widgetPageSave(){
	//페이지저장?
	var owner = $("#owner").val();
	var templatename = $("#templatename").val();
	var version = $("#version").val();
	
	var pageSaveflag = 0;
	function pageSave() {
		
		$("#menuWrapper").hide();
		$("#homepageSaveMenu").hide();
		$("#tools").hide();
		$("#menu").hide();
		$('nav div div *').css("border", "0px dotted");
		$('header div div *').css("border", "0px dotted");
		$('section div div *').css("border", "0px dotted")

		$.ajax({
					url : 'htmlsave',
					type : 'post',
					data : {
						html : document.documentElement.innerHTML,
						owner : $("#owner").val(),
						templatename : $("#templatename").val(),
						version : $("#version").val(),
						encording : '<&#37;&#64; page language=&#34;java&#34; contentType=&#34;text/html; charset=UTF-8&#34; pageEncoding=&#34;UTF-8&#34;%>'
					},
					success : function(data) {
						$("#homepageSaveMenu").show();
						$("#menuWrapper").show();
						$("#tools").show();
						$("#menu").show();
					}
				});

		pageSaveflag = 1;

	}


	function view() {
		if (pageSaveflag == 0) {
			alert("保存ボタンを押してください");
			return false;
		} else {

			location.href = '../makeHomepage/goHomepage?owner='
					+ $("#owner").val() + '&templatename='
					+ $("#templatename").val() + '&version=' + version.value;
			return true;
		}
	}
}
	