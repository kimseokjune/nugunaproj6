function widgetPic() {

	// ////////////////////////////////////////////////////////AA
	$('#AA').draggable({
		start : function(event, ui) {
			$(this).draggable("option", "revert", true);
		}
	});
	$('#BB').draggable({
		start : function(event, ui) {
			$(this).draggable("option", "revert", true);
		}
	});

	// 메뉴중 AA를 드래그했을 때
	$('#AA').on('dragstop', function(event) {
		//버튼이 중복추가되는문제 >> 선삭제 후추가
		$('div[name=AAA] input').remove();
		$('div[name=AAA] label').remove();
		//img를 포함한 div를 append
		$('#innercontent')
				.append(
						'<div id="temp" name="AAA" class="image-container"><img src="../menu/img/defaultpic.png" width="100%" height="100%"></div>');
		$('div[name=AAA]').draggable();
		$('#temp').resizable();
		//첫 위치값 설정(임시아이디를 주고 설정?)
		$('#temp').css({
			"position" : "absolute",
			"top" : event.pageY,
			"left" : event.pageX,
			"z-index" : "5"
		});
		$('div[name=AAA]').each(function(key, el) {
							//각각의 엘레멘트에 ID추가
							$(el).attr('id', 'AAA_' + key);
							//이미지변경/삭제 버튼이 든 div 추가 (안보이게)
							$(el).append(
											'<div name="AAA_option" class="div_option">'
													+ '<label for="imageUp_'+key+'"><img class="imageBtn"></label><input id="imageUp_'+key+'"type="file">'
													+ '<label for="deleteDiv_'+key+'"><img class="deleteBtn"></label><input id="deleteDiv_'+key+'" type="button" value="削除">'
													+ '</div>');
							$('.div_option').css('display','none');
							$('.div_option').children('input').css('display', 'none');
							//각 엘레멘트별 이미지변환 타겟을 지정함 
							$("#imageUp_" + key).on('change',function() {
												if (typeof (FileReader) != "undefined") {
													var reader = new FileReader();
													reader.onload = function(e) {
														$('#AAA_'+key+ ' img:first').attr('src',e.target.result);
													}
													reader.readAsDataURL($(this)[0].files[0]);
												} else {
													alert("This browser does not support FileReader.");
												}
											});
							//각 엘레멘트별 삭제
							$("#deleteDiv_" + key).on('click',function() {
										$('#AAA_' + key).remove();
							})
						});
		//마우스오버시 설정 div 보이기
		$('div[name=AAA]').on('mouseover', function(event) {
			$(this).children('div').show("fast");
		});
		$('div[name=AAA]').on('mouseleave', function(event) {
			$(this).children('div').delay(500).hide("slow");
		});
	});

}