function widgetBoard(){	

$('#wbline').draggable({
		start : function(event, ui) {
			$(this).draggable("option", "revert", true);
		}
	});

	$('#wbline').on('dragstop', function(event) {
		$.post("../owner/widgetboard_list", function(data) {
			$("#innercontent").append(data);
			$("#wbcontent").css({
				"position" : "absolute",
				"top" : event.pageY,
				"left" : event.pageX,
				"z-index" : "999"
			});
			$("#wbcontent").draggable();
			$("#wbcontent").resizable();


		});
	});

	$('#wbnoti').draggable({
		start : function(event, ui) {
			$(this).draggable("option", "revert", true);
		}
	});

	$('#wbnoti').on('dragstop', function(event) {
		$.post("../owner/widgetboard_noti", function(data) {
			$("#innercontent").append(data);
			$("#wbnoticontent").css({
				"position" : "absolute",
				"top" : event.pageY,
				"left" : event.pageX,
				"z-index" : "999"
			});
			$("#wbnoticontent").draggable();
			$("#wbnoticontent").resizable();

		});
	});
	
	
}