<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>UI Elements</title>
  <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel="stylesheet" href="../menu/sidemenu2/css/style.css">
<script src="../menu/js/widgetText.js"></script>
<script src="../menu/js/widgetPic.js"></script>
<script src="../menu/js/widgetBoard.js"></script>
<script src="../menu/js/widgetButton.js"></script>

<link rel="stylesheet" href="../ownerContent/css/jquery.syaku.modal.css">
<script src="../ownerContent/js/jquery.syaku.modal.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<!-- <link rel="stylesheet" href="https://cdn.rawgit.com/AndreaLombardo/BootSideMenu/master/css/BootSideMenu.css"> -->

<link rel="stylesheet" href="../menu/js/BootSideMenu.css">
<script src="../script/toolBoxScroll.js"></script>
<script src="../menu/js/BootSideMenu.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXfkKycQcYx5X9_Y_9_WP1XpINVSElrcM&callback=initMap"></script>

<style type="text/css">


.user {
	padding: 5px;
	margin-bottom: 5px;
}
#videobcg { 
     position: fixed;
     top: 0px;
     left: 0px;
     min-width: 100%;
     min-height: 100%;
     width: auto;
     height: auto;
     z-index: 0;
     overflow: hidden;
}
</style>

<script>
$(function(){
	var owner = $("#owner").val();
	var templatename = $("#templatename").val();
	var version = $("#version").val();
	
	$("#homepageSaveMenus").append('<a href="#" class="btn btn-success btn-rounded" onclick="return view()">화면보기</a><a id="pageSave" onclick="pageSave()" class="btn btn-warning btn-rounded">저장하기</a>');
	/* $("#homepageSaveMenus").append('<a href="../makeHomepage/goHomepage?owner='+owner+'&templatename='+templatename+'&version='+version+'" class="btn btn-success btn-rounded" onclick="return view()">화면보기</a><a id="pageSave" onclick="pageSave()" class="btn btn-warning btn-rounded">저장하기</a>');
	 */
	/* $("#innercontent").append('<h1 id="homeowner">'+ owner + '의 홈페이지</h1>');
	$("#homeowner").draggable(); */
});
	var pageSaveflag = 0;
	function pageSave() {
		
		$("#menuWrapper").hide();
		$("#homepageSaveMenu").hide();
		$("#tools").hide();
		$("#menu").hide();
		$('nav div div *').css("border", "0px dotted");
		$('header div div *').css("border", "0px dotted");
		$('section div div *').css("border", "0px dotted")

		$.ajax({
					url : 'htmlsave',
					type : 'post',
					data : {
						html : document.documentElement.innerHTML,
						owner : $("#owner").val(),
						templatename : $("#templatename").val(),
						version : $("#version").val(),
						encording : '<&#37;&#64; page language=&#34;java&#34; contentType=&#34;text/html; charset=UTF-8&#34; pageEncoding=&#34;UTF-8&#34;%>'
					},
					success : function(data) {
						$("#homepageSaveMenu").show();
						$("#menuWrapper").show();
						$("#tools").show();
						$("#menu").show();
					}
				});

		pageSaveflag = 1;

	}


	function view() {
		if (pageSaveflag == 0) {
			alert("저장하기를 눌러주세요!");
			return false;
		} else {

			location.href = '../makeHomepage/goHomepage?owner='
					+ $("#owner").val() + '&templatename='
					+ $("#templatename").val() + '&version=' + version.value;
			return true;
		}
	}
</script>

<script type="text/javascript">
/* 툴박스는 맨위에있어야 잘작동됩니다. 위치움직이지마세요  */
$(document).ready(function() {

		$('#toolbox').BootSideMenu({
			side : "left",
			autoClose : false
		});
		var e;
		scrollControl(); 
		widgettext();
		widgetPic();
		widgetBoard();
		widgetButton();
		backgroundCH(e);
});
/* 툴박스는 맨위에있어야 잘작동됩니다. 위치움직이지마세요  */
</script>

<script>
$('#ownermaps').draggable({
	start : function(event, ui) {
		$(this).draggable("option", "revert", true);
	}
});

$('#ownermaps').on('dragstop', function(event) {
	$.post("../ownerContent/widgetMap/widgetMap.jsp", function(data) {
		$("#innercontent").append(data);
		   $("#mapsgo").css({
			   "position" : "absolute",
			   "top" : event.clientY,
			   "left" : event.clientX,
			  
			   });
	});
});

</script>
  
</head>

<body>
<div id="toolBox">
  <div class="wrap">
    <section id="vert-nav" >
       <nav role='navigation' >
       
      	<div class="user">
     		<h3 align="center"><a href="../index/index"><img src="../menu/side_logo.png"></a></h3><p>
     		 <h4 class="navbar-link" style="text-align: center;">SETTING</h4>
     		   <ul id="homepageSaveMenus" style="text-align: center;"></ul>
   		</div>
   		<ul id="ulmenu" class="topmenu">
				<li><a href="#0">템플릿▶</a>
 			<ul class="submenu">
    			<li><a href="#0" onclick="backgroundCH('tem1')" >템플릿1</a></li>
    			<li><a href="#0" onclick="backgroundCH('tem2')" >템플릿2</a></li>
    			<li><a href="#0" onclick="backgroundCH('tem3')" >템플릿3</a></li>
    			<li><a href="#0" onclick="backgroundCH('tem4')" >템플릿4</a></li>
    		</ul></li>
   				<li><a href="#subTest">배경위젯▶</a>
   				 <ul class="submenu">
   	 				<li><a href="#" id="backgroundCH" onclick="backgroundCH('bg1')">배경교체</a></li>
     				<li><a href="#">취소</a></li>
   				 </ul></li>
   				 <li><a href="#0">게시판위젯▶</a>
			<ul class="submenu">
				<li id="wbnoti"><a href="##">공지사항</a></li>
				<li id="wbline"><a href="##">한줄평</a></li>	
				
			</ul></li>
				<li  id="AA"><a href="##">사진위젯</a></li>
				<li id="BB"><a href="##">텍스트위젯</a></li>
			
				
			<li id="buttons"><a href="##">버튼</a></li>	
 			
		   	<li>
		   	
		<li><a href="#" id="ownerMaps">지도위젯</a></li>
       <li><a href="#" id="widgetclock">시계위젯</a></li>
       <li><a href="#" ><input type="color" id="coco"/></a></li>
		  </ul> 
    </nav>
    </section>
    </div>
    </div>
    


      <script src="../menu/sidemenu2/js/index.js"></script>
      
  <!--     
   		메뉴 시작
        <ul class="topmenu"  id="ulmenu">
          <li id="AA"><a href="#">사진위젯</a></li>
          <li><a href="#0"><i class="entypo-user"></i> About</a>

          </li>
          <li><a href="#0"><i class="entypo-brush"></i> Projects</a>
            <ul class="submenu">
            <li> <a href="#0">Wordpress</a></li>
             <li> <a href="#0">Case Studies</a></li>
              <li> <a href="#0">Products</a></li>
          </ul>
          </li>
          <li><a href="#0"><i class="entypo-vcard"></i> Contact Us</a></li>
        </ul>
      </nav>  
    </section>
    
  </div>
</div> -->
     <!--   <ul class="submenu">
            <li> <a href="#0">Wordpress</a></li>
             <li> <a href="#0">Case Studies</a></li>
              <li> <a href="#0">Products</a></li>
          </ul> -->

</body>
</html>
