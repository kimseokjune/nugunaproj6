<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript">
$( function() {
	
	$("#fontsize").on("click",function(){
		$("#co").css("opacity", $("#fontsize").val());
	});
	
	$("#colorch").on("onrowenter",function(){
		var colortype = $("#colorch").val();
		alert(colortype);
		$("#co").css("background-color",$("#colorch").val());
	});
	
});
</script>

<style>
#co{
width:100px;
height: 100px;
	background-color:red;
	opacity: 10;
}
</style>
<title>Insert title here</title>
</head>
<body>
<div>
<input type="color" id="colorch" />
<input type="range" step="0.1" min="0.1" max="1" id="fontsize" onclick=""/>
<input type="number" value=""  step="1" min="0" max="10"/>

<input type="text" id="txts" value=""/>
</div>
<div id="co"></div>

</body>
</html>