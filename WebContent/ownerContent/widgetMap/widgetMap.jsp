<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<style>
/* .google-maps {
  position: relative;
  padding-bottom: 75%;
  height: 0;
  overflow: hidden;
}
.google-maps iframe {
  position: absolute;
  top: 0;
  left: 0;
  width: 100% !important;
  height: 100% !important;
} */
#map-canvas {
  width: 150px;
  height: 300px;
  resize:both;
  position:relative;
  padding:5px;
  z-index: 99999;
}
#map-canvas:after{
content: " ";
 display: inline-block;
height: 100%;
left: 0;
opacity: 0;
top: 0;
width: 97%;
z-index: 99999;

}


</style>

<script async defer
   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXfkKycQcYx5X9_Y_9_WP1XpINVSElrcM&callback=initMap">
   
</script>

 <script type="text/javascript">


$("#map-canvas").draggable();   
 /* $("#map-canvas").resizable(); 
  $(function(){
   $('#map-canvas')
      .draggable(); 
       .resizable();  
}); */
   </script>

<script type="text/javascript">
   var id = $("#hosting_id").val();
        var address = $("#shopaddr").val();
   
   var map;
   function initMap() {
   
      var mapOptions = {
         zoom : 18, // 지도를 띄웠을 때의 줌 크기
         mapTypeId : google.maps.MapTypeId.ROADMAP
      };

      var map = new google.maps.Map(document.getElementById("map-canvas"), // div의 id과 값이 같아야 함. "map-canvas"
      mapOptions);

      var size_x = 40; // 마커로 사용할 이미지의 가로 크기
      var size_y = 40; // 마커로 사용할 이미지의 세로 크기

      // 마커로 사용할 이미지 주소
      var image = new google.maps.MarkerImage(address, new google.maps.Size(
            size_x, size_y), '', '', new google.maps.Size(size_x, size_y));

      // Geocoding *****************************************************
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({
         'address' : address
      },
            function(results, status) {
               if (status == google.maps.GeocoderStatus.OK) {
                  map.setCenter(results[0].geometry.location);
                  marker = new google.maps.Marker({
                     map : map,
                     icon : image, // 마커로 사용할 이미지(변수)
                     title : address, // 마커에 마우스 포인트를 갖다댔을 때 뜨는 타이틀
                     position : results[0].geometry.location
                  });
                  // 마커를 클릭했을 때의 이벤트. 말풍선 뿅~
                  google.maps.event.addListener(marker, "click",
                        function() {
                           infowindow.open(map, marker);
                        });
               } else {
                  var inputString = prompt('주소가 존재하지 않습니다 다시 입력해 주세요',
                        '기본 값 문자열');
                  newMap(inputString);

               }
            });
      // Geocoding // *****************************************************

   }

   function newMap(newaddress) {

      var mapOptions = {
         zoom : 18, // 지도를 띄웠을 때의 줌 크기
         mapTypeId : google.maps.MapTypeId.ROADMAP
      };

      var map = new google.maps.Map(document.getElementById("map-canvas"), // div의 id과 값이 같아야 함. "map-canvas"
      mapOptions);

      var size_x = 40; // 마커로 사용할 이미지의 가로 크기
      var size_y = 40; // 마커로 사용할 이미지의 세로 크기

      // 마커로 사용할 이미지 주소
      var image = new google.maps.MarkerImage(newaddress,
            new google.maps.Size(size_x, size_y), '', '',
            new google.maps.Size(size_x, size_y));

      // Geocoding *****************************************************
      var address = newaddress; // DB에서 주소 가져와서 검색하거나 왼쪽과 같이 주소를 바로 코딩.
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({
         'address' : address
      },
            function(results, status) {
               if (status == google.maps.GeocoderStatus.OK) {
                  map.setCenter(results[0].geometry.location);
                  marker = new google.maps.Marker({
                     map : map,
                     icon : image, // 마커로 사용할 이미지(변수)
                     title : address, // 마커에 마우스 포인트를 갖다댔을 때 뜨는 타이틀
                     position : results[0].geometry.location
                  });

                  // 마커를 클릭했을 때의 이벤트. 말풍선 뿅~
                  google.maps.event.addListener(marker, "click",
                        function() {
                           infowindow.open(map, marker);
                        });
               } else {
                  var inputString = prompt('주소가 존재하지 않습니다 다시 입력해 주세요',
                        '기본 값 문자열');
                  newMap(inputString);

               }
            });
      // Geocoding // *****************************************************

   }
   google.maps.event.addDomListener(window, 'load', initialize);
	     $.ajax({
	      url : "newAddress",
	      type : "post",
	      data : {
	         "shopaddr" : address,
	         "hosting_id" : id
	      },
	      success : function(data) {
	         
	      }//end success function
	   });//end ajax 
   
</script>


<!--잘못된 주소 입력시-->

</head>
<body>


   <div id="map-canvas" style="width: 100%; height: 340px"
      title="가게 위치입니다."></div>



<!-- <script src="../script/jquery-3.1.0.min.js"></script> -->
</body>
</html>