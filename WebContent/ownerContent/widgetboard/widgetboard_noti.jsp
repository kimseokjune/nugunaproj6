<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">

<script type="text/javascript">

var owner='';
$( function() {
var Request = function() {  
    this.getParameter = function(name) {  
        var rtnval = '';  
        var nowAddress = unescape(location.href);  
        var parameters = (nowAddress.slice(nowAddress.indexOf('?') + 1,  
                nowAddress.length)).split('&');  
        for (var i = 0; i < parameters.length; i++) {  
            var varName = parameters[i].split('=')[0];  
            if (varName.toUpperCase() == name.toUpperCase()) {  
                rtnval = parameters[i].split('=')[1];  
                break;  
            }  
        }  
        return rtnval;  
    }  
}  
var request = new Request();  
$("#email").attr("value",request.getParameter('owner'));
owner = request.getParameter('owner');
/* alert(request.getParameter('version')); */
});
</script>
<script type="text/javascript">

$( function() {
    $.ajaxSettings.traditional = true;
    $.ajax({
  		url: "../owner/wb_listnoti"
  	    ,type: "post"
  	    ,data: {  
  				"board.email": $("#hosting_id").val()  //나중에 세션으로 아이디 입력
  				,"board.wbtype": "公知事項"

  			}  
  		,success: function(data){ // jason 리절트 값이 data에 들어감
  			$("#wbtablenoti div").remove();
  				var ttalist = data.list;
  				for(var i in ttalist){
  					/* if($("#email").val() == $("#owner").val()){ */
 						$("#wbtablenoti").append('<div class="list-group-item" id="atr"><tr><td>'+ ttalist[i].writer +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ ttalist[i].content +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ ttalist[i].inputdate +'</td><td>&nbsp;&nbsp;&nbsp;<a href="javascript:notiboarddel('+ ttalist[i].wbno +')">삭제</a></td></tr></div>');
 	 			/* 	}else{
 	 					$("#wbtablenoti").append('<div class="list-group-item" id="atr"><tr><td>'+ ttalist[i].writer +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ ttalist[i].content +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ ttalist[i].inputdate +'</td></tr></div>');
 	 	 			} */
  	  		}
  		}
	});

    $("#wbnoticontent").on('mouseover',function(event){
    	$("#notimenu").css('display','inline');
	   
    });
    $("#wbnoticontent").on('mouseleave',function(event){
    	$("#notimenu").css('display','none');
    }); 
    
});



function notiboarddel(wbno){
  $.ajaxSettings.traditional = true;
  $.ajax({
		url: "../owner/widgetboard_up"
	    ,type: "post"
	    ,data: {    
				"wbno": wbno
				,"board.email": $("#hosting_id").val()
 				,"board.wbtype": "公知事項"
 				,"board.wbpass": "1"

			}  
  ,success: function(data){ // jason 리절트 값이 data에 들어감
	  	$("#wbtablenoti div").remove();
		var ttalist = data.list;
			for(var i in ttalist){
			/* 	if($("#email").val() == $("#owner").val()){ */
					$("#wbtablenoti").append('<div class="list-group-item" id="atr"><tr><td>'+ ttalist[i].writer +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ ttalist[i].content +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ ttalist[i].inputdate +'</td><td>&nbsp;&nbsp;&nbsp;<a href="javascript:notiboarddel('+ ttalist[i].wbno +')">삭제</a></td></tr></div>');
			/* 	}else{
					$("#wbtablenoti").append('<div class="list-group-item" id="atr"><tr><td>'+ ttalist[i].writer +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ ttalist[i].content +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ ttalist[i].inputdate +'</td></tr></div>');
	 			} */
		 }	
  		}
	});
}

 function notiwidgetboardinsert(){
  $.ajaxSettings.traditional = true;
  $.ajax({
		url: "../owner/widgetboardinsert"
	    ,type: "post"
	    ,data: {    
			 "board.email": $("#hosting_id").val()
			,"board.writer": $("#wbnotiwriter").val()
			,"board.content": $("#wbnoticontents").val()
			,"board.wbtype": "公知事項" 
			,"board.wbpass": "1"

			}  
 			 ,success: function(data){ // jason 리절트 값이 data에 들어감
 				$("#wbtablenoti div").remove();
 				var ttalist = data.list;
 				for(var i in ttalist){
 	 			/* 	if($("#email").val() == $("#owner").val()){ */
 						$("#wbtablenoti").append('<div class="list-group-item" id="atr"><tr><td>'+ ttalist[i].writer +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ ttalist[i].content +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ ttalist[i].inputdate +'</td><td>&nbsp;&nbsp;&nbsp;<a href="javascript:notiboarddel('+ ttalist[i].wbno +')">삭제</a></td></tr></div>');
 	 			/* 	}else{
 	 					$("#wbtablenoti").append('<div class="list-group-item" id="atr"><tr><td>'+ ttalist[i].writer +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ ttalist[i].content +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ ttalist[i].inputdate +'</td></tr></div>');
 	 	 			} */
				 }	
 		  	}
 		});
	
 		
}
 function boardblue(){
		$(".panel-heading").css("background-color","rgba(61,183,204,0.3)");
		$(".panel-heading").css("border","0px");
		$(".panel-heading").css("color","white");
		$(".panel-default").css("background-color","rgba(178,235,244,0.3)");
		$(".panel-default").css("border","0px");
		$("div tr").css("background-color","none");
		$(".panel").css("border","0px");

	 }

		
function boardblack(){
			$(".panel-heading").css("background-color","rgba(6,6,6,0.3)");
			$(".panel-heading").css("border","0px");
			$(".panel-heading").css("color","white");
			$(".panel-default").css("background-color","rgba(6,6,6,0.3)");
			$(".panel-default").css("border","0px");
			$("div tr").css("background-color","none");
			$(".panel").css("border","0px");
		 }

function boarddefault(){
	$(".panel-heading").css("background-color","#ddd");
	$(".panel-heading").css("border","0px");
	$(".panel-heading").css("color","black");
	$(".panel-default").css("background-color","#f5f5f5");
	$(".panel-default").css("border","0px");
	$("div tr").css("background-color","none");
	$(".panel").css("border","0px");
}
function boardred(){
	$(".panel-heading").css("background-color","rgba(169,23,23,0.3)");
	$(".panel-heading").css("border","0px");
	$(".panel-heading").css("color","white");
	$(".panel-default").css("background-color","rgba(223,77,77,0.3)");
	$(".panel-default").css("border","0px");
	$("div tr").css("background-color","none");
	$(".panel").css("border","0px");
}



 
/*  // 위젯 삭제
	function notiexit(){ $("#wbnoticontent").remove();} */
</script>
<style type="text/css">
#notimenu{
	z-index: 2;
	width: 100%;
	height: 30px;
	background-color: rgba(0,0,0,0);
	position: absolute;
	top: -19px;
	left: 88%;
}
#wbnoticontent{
	z-index:99;
	width: 500px;
	height: -27px;
	min-width:400px;
	position: absolute;
		scrollTop:0;
}
#wbicon,#wbiconset{
	width: 25px;
	height: 25px;
}

</style>
<script type="text/javascript">
function notiexit() {
	$("#wbnoticontent").remove();
}

/* function settings(){

	  var modal = $(
			  '<div id="setting" style="width:490px;height:400px;align:center;">'
			 +'<div id="set1" style="width:230px;height:150px;float:left;padding:5px;"><img src="../ownerContent/widgetboard/img/noti_skin1.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="#" class="btn btn-danger btn-rounded" onclick="boardblack()">선택</a></div>'
			 +'<div id="set2" style="width:230px;height:150px;float:left;padding:5px;"><img src="../ownerContent/widgetboard/img/noti_skin_blue.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="#" class="btn btn-danger btn-rounded" onclick="boardblue()">선택</a></div>'
			 +'<div id="set3" style="width:230px;height:150px;float:left;padding:5px;"><img src="../ownerContent/widgetboard/img/noti_skin_default.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="#" class="btn btn-danger btn-rounded" onclick="boarddefault()">선택</a></div>'
			 +'<div id="set4" style="width:230px;height:150px;float:right;padding:5px;"><img src="../ownerContent/widgetboard/img/noti_skin_red.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="#" class="btn btn-danger btn-rounded" onclick="boardred()">선택</a></div></div>'	
	  ).jmodal(); 
	  
		modal.open({ 'buttonClose': true });
		modal.open();
		
	 } */
	 
	 function settings(){
	 $("#wbnoticontent").append(
				  '<div id="setting" style="width:460px;height:350px;background-color:rgba(255,255,255,0.7);top:-150%;left:85%;z-index:9999999;position:absolute;border-radius:20px;">'
				 +'<div id="set1" style="width:230px;height:150px;float:left;padding:20px;text-align:center;"><img src="../ownerContent/widgetboard/img/noti_skin1.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="##" class="btn btn-danger btn-rounded" onclick="boardblack();">선택</a></div>'
				 +'<div id="set2" style="width:230px;height:150px;float:left;padding:20px;text-align:center;"><img src="../ownerContent/widgetboard/img/noti_skin_blue.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="##" class="btn btn-danger btn-rounded" onclick="boardblue();">선택</a></div>'
				 +'<div id="set3" style="width:230px;height:150px;float:left;padding:20px;text-align:center;"><img src="../ownerContent/widgetboard/img/noti_skin_default.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="##" class="btn btn-danger btn-rounded" onclick="boarddefault();">선택</a></div>'
				 +'<div id="set4" style="width:230px;height:150px;float:left;padding:20px;text-align:center;"><img src="../ownerContent/widgetboard/img/noti_skin_red.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="##" class="btn btn-danger btn-rounded" onclick="boardred();">선택</a></div>'	
		 		 +'<div style="text-align:center;"><input type="button" class="btn btn-success btn-rounded" value="닫기" onclick="notisetclose();"/></div></div>'
	 );

}
	 function notisetclose(){
		 $("#setting").remove();
		 
	 }
	 
</script>
</head>
<body>


<div id="wbnoticontent">
<div id="notimenu">
<!-- <img src="../ownerContent/widgetboard/img/setmenu.jpg" style="width:60px;height:30px;position:fixed;"/> -->
<a href="##" style="position:relative;"><img src="../ownerContent/widgetboard/img/settings.jpg" id="wbiconset" onclick="settings();" style="position:relative;"></a>
<a href="javascript:notiexit()" id="notiexit"><img src="../ownerContent/widgetboard/img/exit.jpg" id="wbicon"  style="position:relative;"></a>

</div>
	<input type="hidden" id="email" value=""/>
	<input type="hidden" id="se_email" value='<s:property value="#session.email"/>'/>
	<input type="hidden" id="wbnotiwriter" value='<s:property value="#session.name"/>'/>
<div class="panel panel-default">
        <div class="panel-heading" id="panel-heading">
               <i class="fa fa-bell fa-fw"></i> 公知事項
        </div>
        
	<table>
		<tr class="col-lg-4">
            <div class="panel-body" id="panel-body">
                    <div class="list-group" id="wbtablenoti">
          
           			 </div>
             </div>                 
		</tr>
	</table>
	
	<div style="padding-left: 10%" id="conPan">
	<input type="text" id="wbnoticontents" style="width:79%;"/>
	<button type="button" class="btn btn-outline-primary waves-effect" onclick="notiwidgetboardinsert()">登録</button>
	<p>
	</div>
	
	</div>
</div>



</body>
</html>
