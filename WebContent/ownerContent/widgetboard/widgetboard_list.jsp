<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">

<script type="text/javascript">

var owner='';
$( function() {
var Request = function() {  
    this.getParameter = function(name) {  
        var rtnval = '';  
        var nowAddress = unescape(location.href);  
        var parameters = (nowAddress.slice(nowAddress.indexOf('?') + 1,  
                nowAddress.length)).split('&');  
        for (var i = 0; i < parameters.length; i++) {  
            var varName = parameters[i].split('=')[0];  
            if (varName.toUpperCase() == name.toUpperCase()) {  
                rtnval = parameters[i].split('=')[1];  
                break;  
            }  
        }  
        return rtnval;  
    }  
}  
var request = new Request();  
$("#email").attr("value",request.getParameter('owner'));
owner = request.getParameter('owner');
/* alert(request.getParameter('version')); */
});
</script>
<script type="text/javascript">
$( function() {
    $.ajaxSettings.traditional = true;
    $.ajax({
  		url: "../owner/wb_list"
  	    ,type: "post"
  	    ,data: {    
  				"board.email": $("#hosting_id").val() //나중에 세션으로 아이디 입력
  				,"board.wbtype": "記号"

  			}  
  		,success: function(data){ // jason 리절트 값이 data에 들어감
  		    $("#tablelist div").remove();
  				var talist = data.list;
  				for(var i in talist){
  					$("#tablelist").append('<div class="list-group-item" id="atr"><tr><td>'+ talist[i].writer +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ talist[i].content +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ talist[i].inputdate +'</td><td>&nbsp;&nbsp;&nbsp;<a href="javascript:boarddel('+ talist[i].wbno +')">삭제</a></td></tr></div>');
  			
  	  				}
  		 }	
	});
    $("#wbcontent").on('mouseover',function(event){
    	$("#linemenu").css('display','inline');
	   
    });
    $("#wbcontent").on('mouseleave',function(event){
    	$("#linemenu").css('display','none');
    }); 

});



function boarddel(wbno){
	var delpw = prompt("암호를 입력하세요", "입력");
	alert(delpw);
	 
  $.ajaxSettings.traditional = true;
  $.ajax({
		url: "../owner/linedeleteBoard"
	    ,type: "post"
	    ,data: {    
				"wbno": wbno
				,"board.email": $("#hosting_id").val()
 				,"board.wbtype": "記号"
 				,"wbpass": delpw

			}  
  ,success: function(data){ // jason 리절트 값이 data에 들어감
	  if(data.delres == 1){
	  	$("#tablelist div").remove();
		var talist = data.list;
		for(var i in talist){
			$("#tablelist").append('<div class="list-group-item" id="atr"><tr><td>'+ talist[i].writer +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ talist[i].content +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ talist[i].inputdate +'</td><td>&nbsp;&nbsp;&nbsp;<a href="javascript:boarddel('+ talist[i].wbno +')">삭제</a></td></tr></div>');
			 }	
 	 }else if(data.delres == 0){
			alert("パスワードを再入力してください");
 		 }
  		}
	});
}

function insertCheck(){
	if($("#wbwriter").val().length >= 5){
		alert("名前は5文字以上");
		return false;
	}else if($("#wbpass").val().length >= 8){
		alert("パスワードは8文字以上");
		return false;
	}else if($("#wbcontents").val().length >= 20){
		alert("パスワードは20文字以下");
		return false;
		}
	widgetboardinsert();
}


 function widgetboardinsert(){
  $.ajaxSettings.traditional = true;
  $.ajax({
		url: "../owner/widgetboardinsert"
	    ,type: "post"
	    ,data: {    
			 "board.email": $("#hosting_id").val()
			,"board.writer": $("#wbwriter").val()
			,"board.content": $("#wbcontents").val()
			,"board.wbtype": "記号" /* $("#wbtype").val() */
			,"board.wbpass": $("#wbpass").val()

			}  
 			 ,success: function(data){ // jason 리절트 값이 data에 들어감
 				$("#tablelist div").remove();
 				var talist = data.list;
 				for(var i in talist){
 					$("#tablelist").append('<div class="list-group-item" id="atr"><tr><td>'+ talist[i].writer +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ talist[i].content +'</td><td>&nbsp;&nbsp;｜&nbsp;'+ talist[i].inputdate +'</td><td>&nbsp;&nbsp;&nbsp;<a href="javascript:boarddel('+ talist[i].wbno +')">삭제</a></td></tr></div>');
 							 }	
 		  		}
 			});
 		
}
 
 function boardblue2(){
		$(".panel-heading2").css("background-color","rgba(61,183,204,0.3)");
		$(".panel-heading2").css("border","0px");
		$(".panel-heading2").css("color","white");
		$(".panel-default2").css("background-color","rgba(178,235,244,0.3)");
		$(".panel-default2").css("border","0px");
		$("div tr").css("background-color","none");
		$(".panel").css("border","0px");

	 }

		
function boardblack2(){
			$(".panel-heading2").css("background-color","rgba(6,6,6,0.3)");
			$(".panel-heading2").css("border","0px");
			$(".panel-heading2").css("color","white");
			$(".panel-default2").css("background-color","rgba(6,6,6,0.3)");
			$(".panel-default2").css("border","0px");
			$("div tr").css("background-color","none");
			$(".panel").css("border","0px");
		 }

function boarddefault2(){
	$(".panel-heading2").css("background-color","#ddd");
	$(".panel-heading2").css("border","0px");
	$(".panel-heading2").css("color","black");
	$(".panel-default2").css("background-color","#f5f5f5");
	$(".panel-default2").css("border","0px");
	$("div tr").css("background-color","none");
	$(".panel").css("border","0px");
}
function boardred2(){
	$(".panel-heading2").css("background-color","rgba(169,23,23,0.3)");
	$(".panel-heading2").css("border","0px");
	$(".panel-heading2").css("color","white");
	$(".panel-default2").css("background-color","rgba(223,77,77,0.3)");
	$(".panel-default2").css("border","0px");
	$("div tr").css("background-color","none");
	$(".panel").css("border","0px");
	
}


</script>
<style type="text/css">
#linemenu{
	z-index: 2;
	width: 100%;
	height: 30px;
	background-color: rgba(0,0,0,0);
	position: absolute;
	top: -19px;
	left: 92%;
}
#wbcontent{
	z-index:1;
	width: 500px;
	height: 22%;
	min-width:400px;
	position: absolute;

}
#wbicon,#wbiconset{
	width: 25px;
	height: 25px;
}

</style>
</head>
<script type="text/javascript">
function linemenuexit() {
	$("#wbcontent").remove();
}

/* function settings2(){

	  var modal2 = $(
			  '<div id="setting2" style="width:490px;height:400px;align:center;">'
			 +'<div id="set1_1" style="width:230px;height:150px;float:left;padding:5px;"><img src="../ownerContent/widgetboard/img/noti_skin1.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="#" class="btn btn-danger btn-rounded" onclick="boardblack2()">선택</a></div>'
			 +'<div id="set2_1" style="width:230px;height:150px;float:left;padding:5px;"><img src="../ownerContent/widgetboard/img/noti_skin_blue.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="#" class="btn btn-danger btn-rounded" onclick="boardblue2()">선택</a></div>'
			 +'<div id="set3_1" style="width:230px;height:150px;float:left;padding:5px;"><img src="../ownerContent/widgetboard/img/noti_skin_default.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="#" class="btn btn-danger btn-rounded" onclick="boarddefault2()">선택</a></div>'
			 +'<div id="set4_1" style="width:230px;height:150px;float:right;padding:5px;"><img src="../ownerContent/widgetboard/img/noti_skin_red.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="#" class="btn btn-danger btn-rounded" onclick="boardred2()">선택</a></div></div>'	
	  ).jmodal(); 
	  
		modal2.open({ 'buttonClose': true });
		modal2.open();
		
	 } */
	 
	 function settings2(){
		 $("#wbcontent").append(
					  '<div id="setting2" style="width:460px;height:350px;background-color:rgba(255,255,255,0.7);top:-150%;left:85%;z-index:9999999;position:absolute;border-radius:20px;">'
					 +'<div id="set1_1" style="width:230px;height:150px;float:left;padding:20px;text-align:center;"><img src="../ownerContent/widgetboard/img/noti_skin1.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="##" class="btn btn-danger btn-rounded" onclick="boardblack2()">선택</a></div>'
					 +'<div id="set2_1" style="width:230px;height:150px;float:left;padding:20px;text-align:center;"><img src="../ownerContent/widgetboard/img/noti_skin_blue.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="##" class="btn btn-danger btn-rounded" onclick="boardblue2()">선택</a></div>'
					 +'<div id="set3_1" style="width:230px;height:150px;float:left;padding:20px;text-align:center;"><img src="../ownerContent/widgetboard/img/noti_skin_default.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="##" class="btn btn-danger btn-rounded" onclick="boarddefault2()">선택</a></div>'
					 +'<div id="set4_1" style="width:230px;height:150px;float:left;padding:20px;text-align:center;"><img src="../ownerContent/widgetboard/img/noti_skin_red.jpg" id="boimg" style="width:200px;height:80px;"/><br><br><a href="##" class="btn btn-danger btn-rounded" onclick="boardred2()">선택</a></div>'	
			 		 +'<div style="text-align:center;"><input type="button" class="btn btn-success btn-rounded" value="닫기" onclick="listsetclose()"/></div></div>'
		 );

	}
		 function listsetclose(){
			 $("#setting2").remove();
			 
		 }
		 
</script>
<body>

<!-- 	<input type="hidden" id="wbtype" value=""/>
	등록자<input type="text" id="wbwriter"/>
	비밀번호<input type="text" id="wbpass"/><br>
	내용<input type="text" id="wbcontents"/>
	<a href="javascript:widgetboardinsert()">등록</a>
	<table id="tablelist">
	
	</table>
	</div> -->
	
<div id="wbcontent">	
<div id="linemenu">
<!-- <img src="../ownerContent/widgetboard/img/setmenu.jpg" style="width:60px;height:30px;position:fixed;"/> -->
<a href="##" style="position:relative;"><img src="../ownerContent/widgetboard/img/settings.jpg" id="wbiconset" onclick="settings2()" style="position:relative;"></a>
<a href="javascript:linemenuexit()" id="linemenuexit"><img src="../ownerContent/widgetboard/img/exit.jpg" id="wbicon"  style="position:relative;"></a>

</div>
	<input type="hidden" id="email" value=""/>
	
<div class="panel panel-default">
        <div class="panel-heading" id="panel-heading">
               <i class="fa fa-bell fa-fw"></i> 記号
        </div>
        
	<table>
		<tr class="col-lg-4">
            <div class="panel-body" id="panel-body">
                    <div class="list-group" id="tablelist">
          
           			 </div>
             </div>                 
		</tr>
	</table>
	<div style="padding-left: 10%">
	<input type="text" id="wbcontents" style="width:79%;"/>
	<%-- <s:textarea id="wbcontents" value="" style="width:70%;height:10%;"></s:textarea> --%><button type="button" class="btn btn-outline-primary waves-effect" onclick="insertCheck()">登録</button>
<!-- 	<a href="javascript:widgetboardinsert()">등록</a> --><br><p>
	</div>
	<div style="padding-left: 10%">
	<input type="text" id="wbwriter" placeholder="名前"/>&nbsp;<input type="text" id="wbpass" placeholder="パスワード"/>
	<p>
	</div>
	</div>


</div>




		</body>
</html>
