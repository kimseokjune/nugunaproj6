<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8"/>
		<title>Tutorial: Digital Clock with HTML5 Audio Alarms</title>
		<!-- The main CSS file -->
		<link href="../ownerContent/widgetclock/assets/css/style.css" rel="stylesheet" />

	</head>

	<body>


		<div id="clock" class="light">
			<div class="button-holder">
			<a id="switch-theme" class="button">디자인</a>
		
		
			<div class="display">
				<div class="weekdays"></div>
				<div class="ampm"></div>
				<div class="alarm"></div>
				<div class="digits"></div>
			</div>
		</div>
</div>



        
		<!-- JavaScript Includes -->
	<!-- 	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> -->
		<script src="../ownerContent/widgetclock/assets/js/moment.min.js"></script>
		<script src="../ownerContent/widgetclock/assets/js/script.js"></script>

	</body>
</html>
