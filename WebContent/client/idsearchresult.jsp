<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <link href="../client/clientcss/password.css" rel="stylesheet">
 <script src="../client/script/jquery-3.1.0.min.js" rel="stylesheet"></script>
 

<title>Insert title here</title>
</head>
<body>
<!--   메뉴시작  -->
	<div id="menu">
	<s:include value="../menu/menu.jsp"></s:include>
	</div>
	<!--   메뉴종료  -->

	<!--   본문시작  -->
	<%-- <div id="content">
	<form method="post" id="list">
	<table>
	<tr>
		<td>아이디 :  <s:property value="client.email"></s:property>  <a href="#" onclick="sub('login')">로그인하기</a></td>
	</tr>
	<tr>
		<td>
		본인인증을 통해 비밀번호를 찾을 수 있습니다.
		</td>
	</tr>
	<tr>
		<td>
		<a href="#" onclick="sub('passwordsearch_q')">질문 답변으로 비밀번호 찾기</a>
		</td>
	</tr>
	
	<tr>
		<td>
	<a href="#" onclick="sub('joinform')">회원가입 진행하기</a>
		</td>
	</tr>
</table>
</form>
	</div>
	<!--   본문종료  --> --%>
	
	<div id="content">
		<div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="text-center">
                  <h3><i class="fa fa-lock fa-4x"></i></h3>
                  <p>検索したアイディーです。</p>
                  <div class="panel-body">
    
    <s:if test="client.password != null">
                      <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                          <input id="id" name="email"  class="form-control" readonly="readonly" type="text" value='<s:property value="client.email"></s:property>'>
                          </s:if>
                          
                        </div>
                      </div>
                      <div class="form-group">
                      	<a href="../client/login">ログイン</a> | <a href="../client/main">メインページ</a>
                      </div>
                      
                    </form>
    
                  </div>
                </div>
              </div>
            </div>
          </div>
	</div>
</body>