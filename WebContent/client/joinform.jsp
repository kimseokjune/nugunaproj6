<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<script src="../client/script/jquery-3.1.0.min.js"></script>
<script>
	$(document).ready(function() {
		var checkTimeOut;
		$('#id').keyup(function() {
			clearTimeout(checkTimeOut);
			checkTimeOut = setTimeout(function() {
				if ($('#id').val().length > 6) {
					var id = $('#id').val();
					$.ajax({
						url : "idcheck",
						type : "post",
						data : {
							"client.email" : id
						},
						success : function(data) {
							if (data.result == "ok") {
								$('#check').empty();
								$('#check').append("사용 가능한 아이디 입니다");
							} else {
								$('#check').empty();
								$('#check').append("사용 불가능한 아이디 입니다");
							}
						}//end success function
					});//end ajax

				}//end if문
			}, 1000); //end setTimeout
		});//set keyup

	});//end ready

	function Checkhint(val) {
		var element = document.getElementById('question');
		if (val == '直接入力') {
			element.style.display = 'block';
		}
	}

	function sub() {
		var f1 = document.list;
		var pw1 = f1.password.value;
		var pw2 = f1.password_check.value;
		if (pw1 != pw2) {
			document.getElementById('password').value = "";
			document.getElementById('password_check').value = "";
			document.getElementById('checkpass').style.color = "red";
			document.getElementById('checkpass').innerHTML = "동일한 암호를 입력하세요.";
			return;
		}
		if (pw1 == pw2) {
			document.getElementById('checkpass').style.color = "black";
			document.getElementById('checkpass').innerHTML = "암호가 확인 되었습니다.";
		}

		list.action = "../client/join.action";
		list.submit();
	}
</script>
<head>
<meta charset="utf-8">
<title>Sign Up - Bootsnipp.com</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="../client/clientcss/join.css" rel="stylesheet">
<script src="../client/script/jquery-3.1.0.min.js" rel="stylesheet"></script>

<style type="text/css">
body {
	padding-top: 30px;
}

td {
	padding: 6px;
}

table {
	text-align: center;
}

.test {
	width: 100%
}
</style>

</head>
<body>
	<!--   메뉴시작  -->
	<div id="menu">
		<s:include value="../menu/menu.jsp"></s:include>
	</div>
	<!--   메뉴종료  -->

	<!--   본문시작  -->
	<div id="content">


		<div class="row">
			<div
				class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
				<form method="post" class="form" role="form" name="list" id="list">
					<h2>
						会員加入 <small>It's free and always will be.</small>
					</h2>
					<hr class="colorgraph">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="text" name="email" id="id"
									class="form-control input-lg" placeholder="6~13文字を入力してください" tabindex="1">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<font  id="check"style="color: red"></font>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="password" name="password" id="password"
									class="form-control input-lg" placeholder="パスワード">
									 <font id="checkpass"></font>
							</div>
						</div>
						
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="password" name="password_check" id="password_check"
									class="form-control input-lg" placeholder="パスワード確認">
							</div>
						</div>
					</div>
					<div class="form-group">
						<input type="text" name="client.name" id="client.name"
							class="form-control input-lg" placeholder="名前">
					</div>
					<div class="form-group">
						<input type="text" name="client.address" id="client.address"
							class="form-control input-lg" placeholder="アドレス">
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<select class="form-control" name="client.question"
								id="client.question" onchange='Checkhint(this.value)'>
									<option value="no">質問選択</option>
									<option value="自分のあだ名は?">自分のあだ名は?</option>
									<option value="一番好きな本は?">一番好きな本は?</option>
									<option value="一番親しい友達の名前">一番親しい友達の名前</option>
									<option value="私の座右の銘は?">私の座右の銘は?</option>
									<option value="私が尊敬する人は?">私が尊敬する人は?</option>
									<option value="私の車の番号は?">私の車の番号は?</option>
									<option value="直接入力">直接入力</option>
							</select><input type="text" style="DISPLAY: none" id="question"
								name="question" class="form-control input-md" required="">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="text" name="client.answer"
									id="client.answer" class="form-control input-lg"
									placeholder="質問の答弁" tabindex="6">
							</div>
						</div>
					</div>

					<div class="form-group">
						<input type="text" name="client.phone_number" id="client.phone_number"
							class="form-control input-lg" placeholder="携帯番号">
					</div>
					<div class="form-group">
						<input type="text" name="client.license_number" id="client.license_number"
							class="form-control input-lg" placeholder="事業者番号">
					</div>


					<hr class="colorgraph">
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<a href="#" onclick="sub()" class="btn btn-primary btn-block btn-lg">会員加入</a>
						</div>
						
					</div>
				</form>
			</div>
		</div>

	</div>
	<!-- 본문 끝 -->

</body>
</html>









