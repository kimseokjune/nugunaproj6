<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <link href="../client/clientcss/password.css" rel="stylesheet">
 <script src="../client/script/jquery-3.1.0.min.js" rel="stylesheet"></script>
 

<title>Insert title here</title>
</head>
<body>
	<!--   메뉴시작  -->
	<div id="menu">
		<s:include value="../menu/menu.jsp"></s:include>
	</div>
	<!--   메뉴종료  -->

	<!--   본문시작  -->
	<div id="content">
		<!-- <form method="post" id="list">
			<table>
				<tr>
					<td>이름</td>
					<td><input type="text" id="client.name" name="client.name"></td>
				</tr>
				<tr>
					<td>핸드폰 번호</td>
					<td><input type="text" id="client.phone_number"name="client.phone_number"></td>
				</tr>
				<tr>
					<td><a href="#" onclick="sub()">확인</a></td>

				</tr>
			</table>
		</form> -->
		<div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="text-center">
                  <h3><i class="fa fa-lock fa-4x"></i></h3>
                  <h2 class="text-center">アイディーを探す</h2>
                  <p>登録情報を入力してください</p>
                  <div class="panel-body">
    
                    <form id="register-form" role="form" autocomplete="off" class="form" method="post" action="idsearchresult">
    
                      <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                          <input id="client.name" name="client.name" placeholder="名" class="form-control"  type="text">
                          
                        </div>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                          <input id="client.phone_number" name="client.phone_number" placeholder="携帯番号" class="form-control"  type="text">
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="アイディーを探す" type="submit">
                      </div>
                      
                    </form>
    
                  </div>
                </div>
              </div>
            </div>
		
	</div>
	<!--   본문종료  -->
</body>
</html>