<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
<title>My registration form - Bootsnipp.com</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet">
<style type="text/css">
</style>
<<script src="../client/script/jquery-3.1.0.min.js" rel="stylesheet"></script>

<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script>
	
		 
			
		

	
	
	function sub(kind){
		if(kind == "out"){
			if(!confirm("脱退しますか。")){
				return;			
			}
			else{
				
			list.action = "../client/out.action";
			list.submit();
			}
		}
		var password = document.getElementById('password').value;
		var password_check = document.getElementById('password_c').value;
		if(password == '') {
			var element = document.getElementById('pass_none');
			element.style.display = 'block';
			document.getElementById('password').value = "";
			document.getElementById('password_c').value = "";
			return;
		}
		if (password != password_check) {
			var element = document.getElementById('pass_none');
			element.style.display = 'none';
			var element = document.getElementById('pass_diff');
			element.style.display = 'block';
			document.getElementById('password').value = "";
			document.getElementById('password_c').value = "";
			return;
		}
		
		if(kind == "modify"){
		list.action = "../client/modify.action";
		list.submit();
		}
	}
	
	
	
	
	
</script>
</head>
<body>
<!--   메뉴시작  -->
	<div id="menu">
		<s:include value="../menu/menu.jsp"></s:include>
	</div>
	<!--   메뉴종료  -->

	<!--   본문시작  -->
	<div id="content" >
	<form class="form-horizontal" id="list" name="list">
		<fieldset>

			<!-- Form Name -->
			<legend>修正フォーム</legend>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="fn">アイディー</label>
				<div class="col-md-4">
					<input id="email" name="email" type="text"
						 class="form-control input-md"
						required="" readonly="readonly" value="<s:property value="client.email"/>">
				</div>
			</div>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="ln">パスワード</label>
				<div class="col-md-4">
					<input id="password" name="password" type="password"
						 class="form-control input-md" required />

				</div>
			</div>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="cmpny">パスワード確認</label>
				<div class="col-md-4">
					<input id="password_c" name="password_c" type="password"
						 class="form-control input-md"
						required /><font style="DISPLAY: none; color: red" id="pass_diff">パスワードを一致させてください。</font>
							<font style="DISPLAY: none; color: red" id="pass_none">パスワードを入力してください。</font>

				</div>
			</div>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="email">名</label>
				<div class="col-md-4">
					<input id="client.name" name="client.name" type="text"
						class="form-control input-md" value="<s:property value="client.name"/>"  readonly="readonly">

				</div>
			</div>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="add2">携帯番号</label>
				<div class="col-md-4">
					<input id="client.phone_number" name="client.phone_number" type="text"
						placeholder="들어가있음" class="form-control input-md" value="<s:property value="client.phone_number"/>">

				</div>
			</div>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="city">アドレス</label>
				<div class="col-md-4">
					<input id="client.address" name="client.address" type="text"
					 placeholder="들어가있음"	class="form-control input-md" required="" value="<s:property value="client.address"/>">

				</div>
			</div>

			<!-- Text input-->
			<div class="form-group">
				<label class="col-md-4 control-label" for="zip">事業者番号</label>
				<div class="col-md-4">
					<input id="client.license_number" name="client.license_number" type="text"
						placeholder="들어가있음" class="form-control input-md" value="<s:property value="client.license_number"/>"
						required="">

				</div>
			</div>





		


			<!-- Button -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="submit"></label>
				<div class="col-md-4">
					<a href="#"  class="btn btn-primary" onclick="sub('modify')">修正完了</a>
					<a href="#"  class="btn btn-primary" onclick="sub('out')">会員脱退</a>
				</div>
			</div>

		</fieldset>
	</form>
</div>

</body>
</html>
