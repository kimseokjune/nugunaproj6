CREATE TABLE NUGUNA_CLIENT(
email varchar2(50) primary key,  				--이메일 주소(계정ID)
password varchar2(20) not null,   				--계정 비밀번호
name varchar2(20) not null,						--이름
birth_date date not null,							--생일
phone_number varchar2(20) not null,		--핸드폰 번호
address varchar2(100) ,								--주소
license_number varchar2(30) ,					--사업자 번호
question varchar2(50) not null,					--질문
answer varchar2(50) not null					--답변
);

SELECT * FROM NUGUNA_CLIENT where email = 'newlifez' and question = '654' and answer = '654';

