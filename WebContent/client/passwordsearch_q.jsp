<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>비밀번호 찾기</title>
<script>
function Checkhint(val){
	var element=document.getElementById('question');
	if(val=='직접입력'){
			
		   element.style.display='block';
	}
}

function sub(){
	list.action = "../client/password_ok.action";
	list.submit();
}
</script>
<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>Password Reset Form - Bootsnipp.com</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <link href="../client/clientcss/password.css" rel="stylesheet">
    <style type="text/css">
        .form-gap {
    padding-top: 70px;
}
    </style>
    <script src="../script/jquery-3.1.0.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    
   <script>
   $(document).ready(function() {
		var checkTimeOut;
		$('#id').keyup(function() {
			clearTimeout(checkTimeOut);
			checkTimeOut = setTimeout(function() {
				if ($('#id').val().length > 6) {
					var id = $('#id').val();
					$.ajax({
						url : "idcheck",
						type : "post",
						data : {
							"client.email" : id
						},
						success : function(data) {
						
								$('#question').val(data.question);
							
						}//end success function
					});//end ajax

				}//end if문
			}, 1000); //end setTimeout
		});//set keyup

	});//end ready

   
   </script>
   
   
</head>
<body>
<!--   메뉴시작  -->
	<div id="menu">
	<s:include value="../menu/menu.jsp"></s:include>
	</div>
	<!--   메뉴종료  -->

	<!--   본문시작  -->
	<div id="content">
		<div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="text-center">
                  <h3><i class="fa fa-lock fa-4x"></i></h3>
                  <h2 class="text-center">パスワードを探す</h2>
                  <p>登録情報を入力してください</p>
                  <div class="panel-body">
    
                    <form id="register-form" role="form" autocomplete="off" class="form" method="post" action="password_ok">
    
                      <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                          <input id="id" name="email" placeholder="アイディー" class="form-control"  type="text">
                          
                        </div>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                          <input id="question" name="question" placeholder="ハンドルネームが存在すれば、自動入力" class="form-control"  type="text" readonly="readonly">
                          
                        </div>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                          <input id="client.answer" name="client.answer" placeholder="パスワードの答弁" class="form-control"  type="text">
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="パスワードを探す" type="submit">
                      </div>
                      
                    </form>
    
                  </div>
                </div>
              </div>
            </div>
          </div>
<script type="text/javascript">

</script>
</body>
</html>