<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="../client/clientcss/animate.css" rel="stylesheet">
<link href="../client/clientcss/joinsuccess.css" rel="stylesheet">
<title>Insert title here</title>

</head>
<body>
	<!--   메뉴시작  -->
	<div id="menu">
		<s:include value="../menu/menu.jsp"></s:include>
	</div>
	<!--   메뉴종료  -->

	<!--   본문시작  -->
	<div id="content">
	
	<div id="background-wrap"  >
    <div class="x1">
        <div class="cloud"></div>
    </div>
    <span id="animationSandbox" style="display: block;" class="bounce animated">
    <h1 class="site__title mega" style="text-align: center; color: white; font-size: ">会員加入成功</h1>
    <h1 class="site__title mega" style="text-align: center; color: white; font-size: ">ホームページ製作に行ってみましょう</h1>
    </span>
    <div class="x2">
        <div class="cloud"></div>
    </div>
    <div class="x3">
        <div class="cloud"></div>
    </div>
    <div class="x4">
        <div class="cloud"></div>
    </div>
    <div class="x5">
        <div class="cloud"></div>
    </div>
</div>
<br><br><br><br><br><br><br><br><br><br><br>
    <div>
 <a class="login_class" href="../makeHomepage/makeHomepageForm.action">私だけのホームページ作り</a>
 <a class="login_class" href="../index/index.action">Go to Main Page</a>
 </div>
		
	</div>

</body>
</html>