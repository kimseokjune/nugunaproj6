<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <link href="../client/clientcss/password.css" rel="stylesheet">
    <style type="text/css">
        .form-gap {
    padding-top: 70px;
}
    </style>
    <script src="../script/jquery-3.1.0.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>
<body>
<!--   메뉴시작  -->
	<div id="menu">
	<s:include value="../menu/menu.jsp"></s:include>
	</div>
	<!--   메뉴종료  -->

	<!--   본문시작  -->
	<div id="content">
	<%-- <table>
	<tr>
	<s:if test="client.password != null">
		<td>비밀번호는  <s:property value="client.password"></s:property> 입니다</td>
		</s:if>
	</tr>
	<tr>
		<td><a href="../client/login">로그인하기</a></td>
	</tr>
	<tr>
		<td><a href="../client/main">홈으로</a></td>
	</tr>
	
</table> --%>
<div id="content">
		<div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="text-center">
                  <h3><i class="fa fa-lock fa-4x"></i></h3>
                  <p>検索した暗証番号です。</p>
                  <div class="panel-body">
    
    <s:if test="client.password != null">
                      <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                          <input id="id" name="email" placeholder="アイディー" class="form-control" readonly="readonly" type="text" value='<s:property value="client.password"></s:property>'>
                          </s:if>
                          
                        </div>
                      </div>
                      <div class="form-group">
                      	<a href="../client/login">ログイン</a> | <a href="../client/main">メインページ</a>
                      </div>
                      
                    </form>
    
                  </div>
                </div>
              </div>
            </div>
          </div>
	</div>
	<!--   본문종료  -->
</body>
</html>