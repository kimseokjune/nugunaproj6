<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Insert title here</title>
<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="../client/script/jquery-3.1.0.min.js" rel="stylesheet"></script>
<style>
table {
	display: table;
	margin-left: auto;
	margin-right: auto;
	border-spacing: 10px;
}

</style>

</head>
<body>
<!--   메뉴시작  -->
	<div id="menu">
	<s:include value="../menu/menu.jsp"></s:include>
	</div>
	<!--   메뉴종료  -->

	<!--   본문시작  -->
	<div id="content">
		<div class="container" style="margin-top: 30px">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							<strong>ログイン </strong>
						</h3>
						<div
							style="float: right; font-size: 80%; position: relative; top: -10px">
							<a href="../client/idsearch.action">アイディーを探す</a> | <a
								href="../client/passwordsearch_q.action">パスワードを探す</a>
						</div>
					</div>

					<div class="panel-body">
						<form role="form" id="list" name="list" action="../client/login"
							method="post">
							
							
							<s:if test='result.equals("no")'>
								<div class="alert alert-danger" >
									<a class="close" data-dismiss="alert" href="#"></a>ログイン情報を再入力してください。
								</div>
							</s:if>
							
							<s:if test='result == null'>
							</s:if>

							<div style="margin-bottom: 12px" class="input-group">
								<span class="input-group-addon"><i
									class="glyphicon glyphicon-user"></i></span> <input id="email"
									type="text" class="form-control" name="email" 
									placeholder="アイディー" style="width: 280px; ">
							</div>

							<div style="margin-bottom: 12px" class="input-group">
								<span class="input-group-addon"><i
									class="glyphicon glyphicon-lock"></i></span> <input id="password"
									type="password" class="form-control" name="password"
									placeholder="パスワード">
							</div>

							<div class="input-group"></div>
							<input type="submit" class="btn btn-success" value="ログイン">
					</div>


					<hr style="margin-top: 10px; margin-bottom: 10px;">

					<div class="form-group">

						<div style="font-size: 85%">
							まだアイディ-がないですか? <a href="../client/joinform.action"> アカウント作り </a>
						</div>

					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>
	<!--   본문종료  -->
	
</body>
</html>